<?php 
session_start();
if(isset($_SESSION['username'])){

}
else{
 header('location:login');
}
?>
<!-- Header -->
<?php require_once('include/header.php');?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php');?>
   
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php');?>
                    <!-- /user menu -->
					
					<?php require_once('include/side-nav-bar.php');?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        
                    </div>
                </div>
                <!-- /page header -->
		

                <!-- Content area -->
                <div class="content">
							<?php if(isset($_GET['mailsuccess'])){?>
					<div class="alert alert-success">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> Mail Sent Successfully.
					</div>
				<?php }elseif(isset($_GET['mailerror'])){?>
					<div class="alert alert-danger">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Sorry!</strong> Error Sending Mail.
					</div>
				<?php }else{ ?>
				<?php } ?>
                    <!-- Main charts -->
                    
                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    
                    <!-- /dashboard content -->


                   <?php require_once('include/footer.php'); ?>