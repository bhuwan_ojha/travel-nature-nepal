<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Page.php');
require_once('../Class/Places.php');
if (isset($_SESSION['username'])) {

} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php'); ?>

    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php'); ?>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <?php require_once('include/side-nav-bar.php'); ?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> Banners </h4>
                        </div>

                        <div class="heading-elements">

                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Image Manager</li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Form horizontal -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <?php if (isset($_GET['success'])) { ?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> Successfully Updated.
                                </div>
                            <?php } ?>
                            <?php if (isset($_GET['error'])) { ?>
                                <div class="alert alert-danger">
                                    <strong>Sorry!</strong> Unable to Update.
                                </div>
                            <?php } ?>
                            <h5 class="panel-title">Add Banner</h5>
                            <br>



                            <form action="add_banner_process.php" enctype="multipart/form-data" method="POST" role="form">
                                <div class="form-group">
                                    <label class="control-label text-semibold"> Banner Uploads</label>
                                    <div class="form-group">
                                        <label class="control-label "><strong>Content</strong></label>
                                        <input type="text" name="title" class="form-control" placeholder="Content">
                                    </div>
                                    <div>
                                        <input type="file"  name="banner_image" value="" class="file-input-custom" accept="image/*" required>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>


                    <h6 class="content-group text-semibold">
                        <strong>Banner Images</strong>
                        <small class="display-block"></small>
                    </h6>

                    <div class="row">
                        <?php
                        $objImage = new Image();
                        $data = $objImage->viewBannerImage();
                        if ($data != 0) {
                            foreach ($data as $key => $value) {
                                ?>
                                <div class="col-lg-3 col-sm-6">
                                    <?php if($value->status==1){ ?>
                                    <div class="thumbnail" style="border-color: red">
                                        <div class="thumb" >

                                            <img src="../uploads/Banner/<?php echo $value->image; ?>" style="height:180px;border-color: limegreen" alt="">
                                            <?php }else{ ?>
                                            <div class="thumbnail">
                                                <div class="thumb" >
                                            <img src="../uploads/Banner/<?php echo $value->image; ?>" style="height:180px;" alt="">
                                            <?php } ?>
                                            <div class="caption-overflow">
                                                <span>
                                                    <a href="../uploads/Banner/<?php echo $value->image; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded" data-toggle="tooltip" data-placement="top" title="View Banner Image"><i class="glyphicon glyphicon-eye-open"></i></a>

                                                    <a href="ajax/banner_status.php?id=<?php echo $value->id; ?>&&status=<?php echo $value->status;?>"  class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5" data-toggle="tooltip" data-placement="top" title="Activate/ Deactivate Banner Image"><i class="glyphicon glyphicon-pencil"></i></a>

													<a href="delete_banner.php?id=<?php echo $value->id; ?>"  class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"  data-toggle="tooltip" data-placement="top" title="Delete Banner Image"><i class="glyphicon glyphicon-trash"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <form action="assign-gallery.php" method ="POST">
                                    <button type="submit" class="btn btn-success">Assign To </button>
                                            <input type="hidden" value="<?php echo $value->image;?>" name="image">
                                        <select class="form-control"  name="location" style="width: 180px;margin-top: -36px;margin-left: 100px;" id="sel1">
                                            <option value="">Select a Destination</option>
                                            <?php
                                            $objPlace = new Places();
                                            $data = $objPlace->viewAllPlaces();
                                            if ($data != 0) {
                                                foreach ($data as $key => $value) {
                                                ?>
                                            <option value="<?php echo $value->slug;?>"><?php echo $value->destination_name; ?></option>
                                            <?php }}?>
                                        </select>
                                        </form>

</div>

                                </div>
                            <?php }
                        } ?>
                    </div>
                  <script type="text/javascript">
				function changeStatus(id){

					$.ajax({
						type: 'GET',
						url: 'ajax/banner_status.php?id='+id,
						success: function(data) {
							alert(data);
							$("p").text(data);

						}
					});
		   }
</script>







                    <!-- /form horizontal -->


<?php require_once('include/footer.php'); ?>

