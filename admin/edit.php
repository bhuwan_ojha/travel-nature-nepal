<?php 
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Page.php');
if(isset($_SESSION['username'])){
}
else{
 header('location:login.php');
}
?>

<?php require_once('include/header.php');?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php');?>
	
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<?php require_once('include/user_menu.php');?>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php require_once('include/side-nav-bar.php');?>

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Page Manager</h4>
						</div>

						<div class="heading-elements">
							
						</div>
					</div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Page Manager</li>
                        </ul>
                    </div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
						<?php if(isset($_GET['success'])){?>
							<div class="alert alert-success">
								<strong>Success!</strong> Successfully Updated.
							</div>
						<?php } ?>
						<?php if(isset($_GET['error'])){?>
							<div class="alert alert-danger">
								<strong>Sorry!</strong> Unable to Update.
							</div>
						<?php } ?>
							<h5 class="panel-title">Add Page</h5>
							<br>
							
								<?php
								$pagename=isset($_GET['pagename'])?$_GET['pagename']:'';
							
                                $objPage = new Page();
                                $data = $objPage->ViewPageContents();                                
                                if ($data != 0) {
                                foreach ($data as $key => $value) {
                                ?>
                              	<?php 
                              	if($pagename=="Home"){?>
							<form action="add_pages_process.php?id=<?php echo $value->id;?>" enctype="multipart/form-data" method="POST" role="form">
							<div class="form-group">
								<label class="control-label "> Page Name</label>
								<input type="text" name="page_name" class="form-control" value="<?php echo $value->page_name;?>">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success" > Save </button>
							</div>	
							<?php } else{?>
							
						<form action="add_pages_process.php?id=<?php echo $value->id;?>" enctype="multipart/form-data" method="POST" role="form">
							<div class="form-group">
								<label class="control-label "> Page Name</label>
								<input type="text" name="page_name" class="form-control" value="<?php echo $value->page_name;?>">
							</div>
						<div class="col-lg-3">
							<div class="thumbnail">
								<div class="thumb">
                                    <?php if($value->image!=''){?>
									<img src="../uploads/PageImage/<?php echo $value->image;?>"  alt="">
                                    <?php }else{?>
                                        <img src="assets/images/image-sample.jpg"  alt="">
                                   <?php  } ?>
									<div class="caption-overflow">
										<span>
											<a href="../uploads/PageImage/<?php echo $value->image;?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
											<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
										</span>
									</div>
								</div>
							</div>
							</div>
                            <div class="clearfix">
							<div class="form-group">

									<div class="form-group">
										<input type="file"  name="image_file" value="<?php echo $value->image;?>" class="file-input-custom" accept="image/*">
										<span class="help-block"></span>
									</div>
								</div>
                            </div>
								
								
								
						
						
							
							
									
							<div class="form-group">
								<label class="control-label ">  Page Description</label>								
								<textarea name="editor1" id="editor1" rows="10" cols="80">
								 <?php echo $value->page_desc;?>
								</textarea>
								<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( 'editor1' );
								</script>
							</div>	
							<div class="form-group">
								<button type="submit" class="btn btn-success" > Save </button>
							</div>			
							
							</form>
								<?php }}} ?>
						</div>
						</div>

						
					<!-- /form horizontal -->

					
					<?php require_once('include/footer.php');?>					