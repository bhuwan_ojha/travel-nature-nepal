<?php 
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Seo.php');
if(isset($_SESSION['username'])){
}
else{
 header('location:login.php');
}
?>

<?php require_once('include/header.php');?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php');?>
	
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<?php require_once('include/user_menu.php');?>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php require_once('include/side-nav-bar.php');?>

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Basic Inputs</h4>
						</div>

						<div class="heading-elements">
							
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
							
							<li class="active">Edit Seo Manager</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
						<?php if(isset($_GET['updatesuccess'])){?>
							<div class="alert alert-success">
								<strong>Success!</strong> Successfully Updated.
							</div>
						<?php }elseif(isset($_GET['add_success'])){	?>
							<div class="alert alert-success">
								<strong>Success!</strong> Successfully Added.
							</div>
						<?php } elseif(isset($_GET['updateerror'])){?>
							<div class="alert alert-danger">
								<strong>Sorry!</strong> Unable to Update.
							</div>
						<?php }elseif(isset($_GET['failed'])){	?>
							<div class="alert alert-danger">
								<strong>Sorry!</strong> Unable to Add.
							</div>
						<?php } ?>
						
						
							 <div class="content">
							<?php if(isset($_GET['id'])){?>

                            <h5 class="panel-title">SEO Manager | Update</h5>
<br>
            <div class="box-body">
           					    <?php
                                $objSeo = new Seo();
                                $data = $objSeo->ViewSeoContents();                                
                                if ($data != 0) {
                                foreach ($data as $key => $value) {
                                ?>
                <form action="seo_update.php?id=<?php echo $value->id;?>" method="post" class="forms">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>
                                <label>Page Name<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page name" class="form-control required" name="page_name" value="<?php echo $value->name;?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Link<span class="text-danger">*</span></label>
                                <input type="text" name="link" placeholder="Enter url for seo (leave blank for home page)" class="form-control" value="<?php echo $value->link;?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Page Title<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page title" class="form-control required" name="title" value="<?php echo $value->title;?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Description<span class="text-danger">*</span></label>
                                <textarea name="description" placeholder="Enter page description" class="form-control required"><?php echo $value->description;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Keywords<span class="text-danger">*</span></label>
                                <textarea name="keywords" placeholder="Enter keywords (comma separated)" class="form-control required"><?php echo $value->keywords;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-success" type="submit" value="Save">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
                <?php }}?>
            </div>
<?php }else{ ?>

   <h5 class="panel-title">SEO Manager | Add</h5>
<br>
            <div class="box-body">
                <form action="add_seo.php" method="post" class="forms">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>
                                <label>Page Name<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page name" class="form-control required" name="page_name">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Link<span class="text-danger">*</span></label>
                                <input type="text" name="link" placeholder="Enter url for seo (leave blank for home page)" class="form-control" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Page Title<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page title" class="form-control required" name="title" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Description<span class="text-danger">*</span></label>
                                <textarea name="description" placeholder="Enter page description" class="form-control required"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Keywords<span class="text-danger">*</span></label>
                                <textarea name="keywords" placeholder="Enter keywords (comma separated)" class="form-control required"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-success" type="submit" value="Save">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <?php }?>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->



                            </table>
                        </div>
                    </div>


                    <!-- /form horizontal -->


                    <?php require_once('include/footer.php'); ?>

					