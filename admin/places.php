<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Page.php');
if (isset($_SESSION['username'])) {
    
} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php'); ?>

    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php'); ?>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <?php require_once('include/side-nav-bar.php'); ?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Basic Inputs</h4>
                            <button type="button"  style="margin-top: -15px" class="btn btn-success pull-right" onclick="window.location.href='<?php echo BASE_URL;?>admin/edit-place'">View | Edit</button>
                        </div>

                   
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="#">Forms</a></li>
                            <li class="active">Place Manager</li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Form horizontal -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <?php if (isset($_GET['success'])) { ?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> Successfully Added.
                                </div>
                            <?php } ?>
                            <?php if (isset($_GET['error'])) { ?>
                                <div class="alert alert-danger">
                                    <strong>Sorry!</strong> Unable to Add.
                                </div>
                            <?php } ?>
                            <h5 class="panel-title">Add Places</h5>

                            <br>
							<?php 
							require_once'../Class/Places.php';
							$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
							$uri_segments = explode('/', $uri_path);
							$id=$uri_segments[3];	
							
							if($id){
								$objPlace=new Places();
								$data=$objPlace->viewAllPlacesById();
								if($data!=0){
									foreach($data as $value){
										?>

                            <form action="<?php echo BASE_URL;?>admin/places_process.php?id=<?php echo $value['id'];?>" enctype="multipart/form-data" method="POST" role="form">
                                <div class="form-group">
                                    <label class="control-label ">Place Name</label>
                                    <input type="text" name="place_name" class="form-control" Placeholder="Place Name" required="" value="<?php echo $value['destination_name'];?>">
                                </div>
                                <div class="form-group">
                                    <label class="display-block text-semibold">Select Category</label>
                                    <select class="form-control" name="is_what" id="editsel1">
                                        <option value="">Select Category</option>
                                        <?php
                                        $objPlace=new Places();
                                        $newdata=$objPlace->getCategoryById();
                                        print_r($newdata);
                                        if($data!=0){
                                            foreach ($newdata as $newvalue) { ?>
                                                <option value="<?php echo $newvalue->category;?>" selected ><?php echo $newvalue->category;?></option>
                                            <?php }} ?>
                                        <option id="editother"  value="other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group" id="editotherCategory">
                                    <label class="control-label ">Add New Category</label>
                                    <input type="text" name="is_what1" class="form-control"  Placeholder="Other" >
                                </div>

                                <div class="form-group">
                                    <label class="control-label text">Image Uploads</label>
                                    <div>
                                        <img src="<?php echo BASE_URL;?>uploads/Places/<?php echo $value['image'];?>" style="height: 100px;margin-bottom: 15px;">
                                        <input type="file"  name="places" value="<?php echo $value['image'];?>" class="file-input-custom" accept="p/*">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label ">  Page Description</label>								
                                    <textarea name="editor1" id="editor1" rows="10" cols="80">
                                      <?php echo $value['description'];?>
                                    </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace('editor1');
                                    </script>
                                </div>	
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" > Save </button>
                                </div>

                            </form>
							<?php }}}else{ ?>
                            <form action="<?php echo BASE_URL;?>admin/places_process.php" enctype="multipart/form-data" method="POST" role="form">
                                <div class="form-group">
                                    <label class="control-label ">Place Name</label>
                                    <input type="text" name="place_name" class="form-control" Placeholder="Place Name" required="" value="">
                                </div>
                                <div class="form-group">
                                    <label class="display-block text-semibold">Select Category</label>
                                    <select class="form-control" name="is_what" id="sel1">
                                        <option value="">Select Category</option>
                                        <?php
                                        $objPlace=new Places();
                                        $data1=$objPlace->getCategory();
                                        if($data1!=0){
                                            foreach ($data1 as $value) { ?>
                                                <option value="<?php echo $value->category;?>"><?php echo $value->category;?></option>
                                            <?php }} ?>
                                        <option  id="other" value="other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group" id="otherCategory">
                                    <label class="control-label ">Add New Category</label>
                                    <input type="text" name="is_what1" class="form-control" id="otherCategory" Placeholder="Other" >
                                </div>
                                <div class="form-group">
                                    <label class="control-label text">Image Uploads</label>
                                    <div>
                                        <input type="file"  name="places" value="" class="file-input-custom" required="" accept="p/*">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label ">  Page Description</label>								
                                    <textarea name="editor1" id="editor1" rows="10" cols="80">
                                         
                                    </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace('editor1');
                                    </script>
                                </div>	
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" > Save </button>
                                </div>		

                            </form>
							<?php } ?>

                        </div>
                    </div>
                    <script>
                        $(function(){
                            $('#otherCategory').hide();
                            $('#sel1').on('change',function () {
                                if($('#sel1').val()=='other') {
                                    $('#otherCategory').show();
                                }else{
                                    $('#otherCategory').hide();
                                }
                            });
                        });
                    </script>

                    <script>
                        $(document).ready(function(){

                            $('#editotherCategory').hide();
                            $('#editsel1').on('change',function () {
                                if($('#editsel1').val()=='other') {
                                    $('#editotherCategory').show();
                                }else{
                                    $('#editotherCategory').hide();
                                }
                            });
                        });
                    </script>

                    <!-- /form horizontal -->


                    <?php require_once('include/footer.php'); ?>					