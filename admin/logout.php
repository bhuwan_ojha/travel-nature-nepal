<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/User.php');

session_unset();

session_destroy();

header("location:login");

?>