<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Price.php');

if(isset($_SESSION['username'])){
}
else{
 header('location:login.php');
}

$id=isset($_GET['id'])?$_GET['id']:'';
$objPrice=new Price();

$editor1=mysqli_real_escape_string($objPrice->conxn, $_POST['editor1']);
$location_slug=mysqli_real_escape_string($objPrice->conxn, $_POST['location']);
$cost=mysqli_real_escape_string($objPrice->conxn, $_POST['cost']);
$map=mysqli_real_escape_string($objPrice->conxn, $_POST['map']);
$additional_information=mysqli_real_escape_string($objPrice->conxn, $_POST['editor2']);



if($id) {
    $flag = $objPrice->updatePrice($cost,$editor1,$additional_information,$map);
    if ($flag == TRUE) {
        header("location:edit_price.php?id=$id&success");
    } else {
        header("location:edit_price.php?id=$id&error");
    }

}else{
$flag=$objPrice->addPrice($cost,$location_slug,$editor1,$additional_information,$map);
    if($flag==TRUE){
        header("location:edit_price.php?success");
    }
    else{
        header("location:edit_price.php?error");
    }

}

    
    ?>