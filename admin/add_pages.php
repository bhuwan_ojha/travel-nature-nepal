<?php 
session_start();

if(isset($_SESSION['username'])){
}
else{
 header('location:login.php');
}
?>

<?php require_once('include/header.php');?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php');?>
	
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<?php require_once('include/user_menu.php');?>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php require_once('include/side-nav-bar.php');?>

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Basic Inputs</h4>
						</div>

						<div class="heading-elements">
							
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="form_inputs_basic.html">Forms</a></li>
							<li class="active">Basic inputs</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
						<?php if(isset($_GET['success'])){?>
							<div class="alert alert-success">
								<strong>Success!</strong> Page Added.
							</div>
						<?php } ?>
							<h5 class="panel-title">Add Page</h5>
							<br>
							<form action="add_pages_process.php" method="POST" role="form">
							<div class="form-group">
								<label class="control-label "> Page Name</label>
								<input type="text" name="page_name" class="form-control" placeholder="Page Title" required="">
							</div>
							
							
									
							<div class="form-group">
								<label class="control-label ">  Page Description</label>								
								<textarea name="editor1" id="editor1" rows="10" cols="80">
								 
								</textarea>
								<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( 'editor1' );
								</script>
							</div>	
							<div class="form-group">
								<button type="submit" class="btn btn-success" > Save </button>
							</div>			
							
							</form>
						</div>
						</div>

						
					<!-- /form horizontal -->

					
					<?php require_once('include/footer.php');?>
					
					
					