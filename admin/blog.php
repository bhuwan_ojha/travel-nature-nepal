<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Blog.php');
if (isset($_SESSION['username'])) {
    
} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php'); ?>

    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php'); ?>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <?php require_once('include/side-nav-bar.php'); ?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Blog Manager</h4>
                        </div>

                   
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                            
                            <li class="active">Blog manager</li>
                        </ul>

                        
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Form horizontal -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <?php if (isset($_GET['success'])) { ?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> Successfully Added.
                                </div>
                            <?php } ?>
                            <?php if (isset($_GET['error'])) { ?>
                                <div class="alert alert-danger">
                                    <strong>Sorry!</strong> Unable to Add.
                                </div>
                            <?php } ?>
                            <h5 class="panel-title">Add Blog</h5>
							<a href="edit-blog-manager"><button type="button"  class="btn btn-success pull-right">View | Edit</button></a>
                            <br>
							
							<?php 
							require_once'../Class/Blog.php';
							$slug=isset($_GET['slug'])?$_GET['slug']:'';
							if($slug){
								$objBlog=new Blog();
								$data=$objBlog->viewBlogById();
								if($data!=0){
									foreach($data as $value){
										?>
								

                            <form action="blog_process.php?slug=<?php echo $value->slug;?>" enctype="multipart/form-data" method="POST" role="form">
                                <div class="form-group">
                                    <label class="control-label ">Blog Title</label>
                                    <input type="text" name="blog_title" class="form-control" Placeholder="Blog Title"  value="<?php echo $value->blog_title;?>">
                                </div>
                                <div class="form-group">
                                     <label class="control-label">Blog Category</label>
                                          <input type="text" name="category" class="form-control" Placeholder="Blog Category"  value="<?php echo $value->category;?>">                                
                                    </label>

                                   
                                </div>
                                <div class="form-group">
                                    <label class="control-label text">Image Uploads</label>
                                    <div>
                                    <img src="../uploads/<?php echo $value->image;?>" style="height: 100px;margin-bottom: 15px;">
                                        <input type="file" value="" name="image" class="file-input-custom" accept="p/*" >
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                     <label class="control-label">Video URL</label>
                                     <div width="200" height="200">
                                         <?php echo $value->video;?> 
                                         </div>
                                             <div>
                                          <input type="text" name="video" class="form-control" Placeholder="Video Url"  value="">                                
                                    </label>
                                </div>
                                   </div>
                                <div class="form-group">
                                    <label class="control-label ">Blog Description</label>					
                                    <textarea name="description" id="description" rows="10" cols="80">
                                      <?php echo $value->description;?>
                                    </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace('description');
                                    </script>
                                </div>	
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" > Save </button>
                                </div>		

                            </form>
								<?php }}}else{ ?>
								     <form action="blog_process.php" enctype="multipart/form-data" method="POST" role="form">
                                <div class="form-group">
                                    <label class="control-label ">Blog Title</label>
                                    <input type="text" name="blog_title" class="form-control" Placeholder="Blog Title" required="" value="">
                                </div>
                                <div class="form-group">
                                     <label class="control-label">Blog Category</label>
                                          <input type="text" name="category" class="form-control" Placeholder="Blog Category" required="" value="">                                
                                    </label>                                   
                                </div>                           
                                <div class="form-group">
                                    <label class="control-label text">Image Uploads</label>
                                    <div>
                                        <input type="file"  name="image" value="" class="file-input-custom" accept="p/*">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                     <label class="control-label">Video URL</label>
                                          <input type="text" name="video" class="form-control" Placeholder="Blog Category" required="" value="">                                
                                    </label>                                   
                                </div>
                                <div class="form-group">
                                    <label class="control-label ">Blog Description</label>								
                                    <textarea name="description" id="description" rows="10" cols="80">
                                         
                                    </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace('description');
                                    </script>
                                </div>	
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" > Save </button>
                                </div>		

                            </form>
							
							<?php } ?>

                        </div>
                    </div>


                    <!-- /form horizontal -->


                    <?php require_once('include/footer.php'); ?>					