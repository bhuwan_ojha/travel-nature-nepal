<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/User.php');


$objUser=new User();

$username=mysqli_real_escape_string($objUser->conxn, $_POST['username']);
$password=mysqli_real_escape_string($objUser->conxn, SHA1($_POST['password']));


$objUser->setUsername($username);
$objUser->setPassword($password);

$flag=$objUser->Login();

if($flag==TRUE){
     header("Location:dashboard");
}
else{
   header("Location:login?error=1");
}
?>