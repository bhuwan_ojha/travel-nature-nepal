<?php 
session_start();
if(isset($_SESSION['username'])){

}
else{
 header('location:login.php');
}
?>
<!-- Header -->
<?php require_once('include/header.php');?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php');?>
   
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php');?>
                    <!-- /user menu -->
					
					<?php require_once('include/side-nav-bar.php');?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add|Edit</span></h4>
                        </div>

                        <div class="heading-elements">
                            
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active"></li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    Settings
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                    <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->
				

                <!-- Content area -->
                 <div class="content">
                    <!-- Form horizontal -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
						<table id="table_id" class="table datatable-responsive">
							<thead>
								<tr>
									<th>Id</th>
									<th>Place Name</th>
									<th>Cost</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<?php 
							require_once'../Class/Connection.php';
							require_once'../Class/Price.php';
							
							$objPlaces=new Price();
							$data =$objPlaces->getPrice();
							if($data !=0){
								foreach($data as $value){
									?>
							<tbody>
								<tr>
									<td><?php echo $value->id;?></td>
									<td><?php echo $value->destination_name;?></td>
									<td><?php echo $value->cost;?></td>
									<td><a href="pricing.php?slug=<?php echo base64_encode($value->location_slug);?>"><button type="button"  class="btn btn-success">Edit</button></a> &nbsp; <a href="delete_places.php?id=<?php echo $value->id;?>" onclick="return confirm('Are You Sure? Deleted Records Cannot Be Recovered!');"  class="btn btn-danger">Delete</button></a></td>
								</tr>
								
							</tbody>
							<?php }} ?>
						</table>
						
						</div>						
					</div>
				</div>
                    <!-- Main charts -->
                    
                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    
                    <!-- /dashboard content -->


                   <?php require_once('include/footer.php'); ?>
				   
				   <script>
					$(document).ready(function() {
					$('#table_id').DataTable( {
						"pagingType": "full_numbers"
					} );
				} );
				</script>