<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/User.php');
if (isset($_SESSION['username'])) {
    
} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

    <!-- Main navbar -->
    <?php require_once('include/nav-bar.php'); ?>

    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <?php require_once('include/user_menu.php'); ?>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <?php require_once('include/side-nav-bar.php'); ?>

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Basic Inputs</h4>
                        </div>

                        <div class="heading-elements">
                            
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                            
                            <li class="active">Admin Manager</li>
                        </ul>

                        
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Form horizontal -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
						<?php if(isset($_GET['success'])){?>
							<div class="alert alert-success">
								<strong>Success!</strong> Successfully Updated.
							</div>
						<?php } elseif(isset($_GET['error'])){ ?>
						<div class="alert alert-danger">
								<strong>Sorry!</strong> Unable to Update.
							</div>
						<?php } ?>
                            <h5 class="panel-title">Page List</h5>
                            
                            <table class="table datatable-responsive">

                                <thead>
                                    <tr>
										<th>S.N.</th>
                                        <th>Username</th>                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>

                                <?php
                                $objUser = new User();
                                $data = $objUser->showAdmin();                                
                                if ($data != 0) {
                                    foreach ($data as $key => $value) { 
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td><strong><?php echo $value->id; ?>.</strong></td>
                                                <td><strong><?php echo $value->username; ?></strong></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="label label-success"></span></td>
                                                <td class="text-center">
                                                    <a href="#"> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Edit</button></a>
													
                                                  
                                                    </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    <?php }} ?>


                            </table>
                        </div>
                    </div>
					

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Admin User Manager</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="admin_update.php"  method="POST" role="form">

							<div class="form-group">
								<label class="control-label ">Username</label>
								<input type="text" name="username" class="form-control" value="<?php echo $value->username;?>" required="">
							</div>
							      	<div class="form-group">
								<label class="control-label "> Password</label>
								<input type="password" name="password" class="form-control" placeholder="Add New Password" required="">
							</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
		</form>
      </div>
    </div>
  </div>
</div>
					<!--/adminModal -->


                    <!-- /form horizontal -->


                    <?php require_once('include/footer.php'); ?>

					