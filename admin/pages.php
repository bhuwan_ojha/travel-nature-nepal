<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Page.php');

if (isset($_SESSION['username'])) {

} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

<!-- Main navbar -->
<?php require_once('include/nav-bar.php'); ?>

<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <?php require_once('include/user_menu.php'); ?>
                <!-- /user menu -->


                <!-- Main navigation -->
                <?php require_once('include/side-nav-bar.php'); ?>

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span
                                class="text-semibold">ADD|UPDATE:</span> - Page Manager</h4>
                    </div>

                    <div class="heading-elements">

                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">Page Manager</li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Form horizontal -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Page List</h5>
                        <!--<a href="add_pages.php"><button type="button" class="btn btn-success btn-lg pull-right">Add Page</button></a>-->

                        <table class="table datatable-responsive">

                            <thead>
                            <tr>
                                <th>Page Name</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>

                            <?php
                            $objPage = new Page();
                            $data = $objPage->PageList();
                            if ($data != 0) {
                                foreach ($data as $key => $value) {
                                    ?>
                                    <tbody>
                                    <tr>
                                        <td><strong><?php echo $value->page_name; ?></strong></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><span class="label label-success"></span></td>
                                        <td class="text-center">
                                            <a href="edit.php?id=<?php echo $value->id; ?>&&pagename=<?php echo $value->page_name; ?>">
                                                <button type="button" class="btn btn-success">Edit</button>
                                            </a>

                                            <!--<a href="delete_page.php?id=<?php /*echo $value->id;*/ ?>"><button type="button" onclick="return confirm('Are You Sure?')"; class="btn btn-danger">Delete</button></a>-->


                                            </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    </tbody>
                                <?php
                                }
                            } ?>


                        </table>
                    </div>
                </div>


                <!-- /form horizontal -->

                <?php require_once('include/footer.php'); ?>


					