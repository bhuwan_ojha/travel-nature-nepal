<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="dashboard">Travel Nature Nepal - Admin Panel</a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            <li class="dropdown">


            </li>
        </ul>



        <ul class="nav navbar-nav navbar-right">
            <!-- <li class="dropdown language-switch">
                 <a class="dropdown-toggle" data-toggle="dropdown">
                     <img src="<?php echo BASE_URL;?>admin/assets/images/flags/gb.png" class="position-left" alt="">
                     English
                     <span class="caret"></span>
                 </a>
 
                 <ul class="dropdown-menu">
                     <li><a class="deutsch"><img src="assets/images/flags/de.png" alt=""> Deutsch</a></li>
                     <li><a class="ukrainian"><img src="assets/images/flags/ua.png" alt=""> Україн�?ька</a></li>
                     <li><a class="english"><img src="assets/images/flags/gb.png" alt=""> English</a></li>
                     <li><a class="espana"><img src="assets/images/flags/es.png" alt=""> España</a></li>
                     <li><a class="russian"><img src="assets/images/flags/ru.png" alt=""> Ру�?�?кий</a></li>
                 </ul>
             </li>-->
            <?php
            require_once('../Class/Connection.php');
            require_once('../Class/Comment.php');
            $objComment = new Comment();
            $numRows = $objComment->CountComment();
            ?>
            <!--<li class="dropdown">
                <a href="#"   class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-bubbles4"></i>
                    <span class="visible-xs-inline-block position-right">Comment</span>
                    <span  id="notification_count" class="badge bg-warning-400"><?php /*echo $numRows; */?></span>
                </a>-->



                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-heading">
                        Comment
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-compose"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body">
                        <?php
                        require_once('../Class/Connection.php');
                        require_once('../Class/Comment.php');
                        $objComment = new Comment();
                        $data = $objComment->ViewComment();
                        if ($data != 0) {
                            foreach ($data as $key => $value) {
                                ?>
                                <li class="media">
                                    <div class="media-left">
                                        <img src="assets/images/demo/users/face11.jpg" class="img-circle img-sm" alt="">

                                    </div>

                                    <div class="media-body">
                                        <a href="#"   data-toggle="modal" data-target="#<?php echo $value->id; ?>" class="media-heading">
                                            <span class="text-semibold"><?php echo $value->name; ?></span> <small>Commented on</small> <span class="text-semibold" style="color: red"><?php echo $value->blog_title; ?></span>
                                            <span class="media-annotation pull-right"><?php echo $value->commented_date; ?></span>
                                        </a>

                                        <span class="text-muted"><?php echo $value->comment; ?></span>
                                    </div>
                                </li>
    <?php }
} ?>

                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>

            </li>

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/demo/users/face11.jpg" alt="">
                    <span><?php echo $_SESSION['username']; ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<?php
if ($data != 0) {
    foreach ($data as $value) {
        ?>
        <div id="<?php echo $value->id; ?>" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title"><span class="status-mark bg-success position-left"></span><?php echo $value->name; ?></h6>
                    </div>

                    <div class="modal-body">
                        <ul class="media-list chat-list content-group">
                            <li class="media date-step">
                                <span><?php echo $value->email; ?></span>
                            </li>


                            <li class="media">
                                <div class="media-left">
                                    <a href="assets/images/demo/images/3.png">
                                        <img src="assets/images/demo/users/face11.jpg" class="img-circle" alt="">
                                    </a>
                                </div>

                                <div class="media-body">
                                    <div class="media-content"><?php echo $value->comment; ?></div>
                                    <span class="media-annotation display-block mt-10"><?php echo $value->commented_date; ?><a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>
                                </div>

                            </li>



                            </li>								
                        </ul>

                        <form action="message_submit.php" method="POST">
                            <textarea name="content" class="form-control content-group" rows="3" cols="1" placeholder="Enter your message..."></textarea>
                            <input type="hidden" name="replied_id" value="<?php echo $value->id; ?>">
                            <input type="hidden" name="email" value="<?php echo $value->email; ?>">
                           
                               <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit"   onclick="Update(<?php echo $value->id; ?>)" class="btn bg-teal-400 btn-labeled btn-labeled-right" ><b><i class="icon-circle-right2"></i></b> Approve Comment </button>
                                </div>
                                <div class="col-xs-6">
                                    <button type="submit"   onclick="RemoveComment(<?php echo $value->id; ?>)" class="btn bg-teal-400 btn-labeled btn-labeled-right pull-right" ><b><i class="icon-circle-right2"></i></b> Remove Comment </button>
                                    
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    <?php }
} ?>	














<script>
    function Update(id) {
        var count = $('#notification_count').text();
        $.ajax({
            url: "ajax/message_status.php?id=" + id,
            method: "GET",
            success: function (data) {
                console.log($('#notification_count').text());
                $('#notification_count').text(data);
                console.log(data);
                reload.window();
            }
        });
    }

</script>
<script>
    function RemoveComment(id) {
        var count = $('#notification_count').text();
        $.ajax({
            url: "ajax/remove_comment.php?id=" + id,
            method: "GET",
            success: function (data) {
                console.log($('#notification_count').text());
                $('#notification_count').text(data);
                console.log(data);
                reload.window();
            }
        });
    }

</script>

