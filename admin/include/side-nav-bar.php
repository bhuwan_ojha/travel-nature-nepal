<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li ><a href="<?php echo BASE_URL;?>admin/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

            <!-- /main -->

            <!-- Forms -->
            <li class="navigation-header"><span>Content Management System</span> <i class="icon-menu" title="Forms"></i>
            </li>
            <li class="pageManager">
                <a href="#"><i class="icon-pencil3"></i> <span>Page Management</span></a>
                <ul class="contentManager-ul" style="display: none">
                    <li ><a href="<?php echo BASE_URL;?>admin/page-manager">Content Manager</a></li>
                    <li ><a href="<?php echo BASE_URL;?>admin/banner-manager">Image Manager</a></li>
                    <li ><a href="<?php echo BASE_URL;?>admin/place-manager">Places Manager</a></li>
                    <li ><a href="<?php echo BASE_URL;?>admin/pricing">Pricing</a></li>

                    <!--<li><a href="Blog.php">Blog Manager</a></li>-->


            </li>
        </ul>


        <li class="navigation-header"><span>SEO Tools Integration</span> <i class="icon-menu" title="Forms"></i>
        </li>
        <li >
            <a href="#"><i class="icon-search4"></i> <span>SEO Tools</span></a>
            <ul  style="display: none">
                <li ><a href="<?php echo BASE_URL;?>admin/seo-manager">SEO Manager</a></li>
                <li ><a href="<?php echo BASE_URL;?>admin/common-seo-meta-tags">Common SEO Meta</a></li>


        </li>
        </ul>

        <li class="navigation-header"><span>Admin User Manager</span> <i class="icon-menu" title="Forms"></i>
        </li>
        <li>
        <li ><a href="<?php echo BASE_URL;?>admin/admin-manager"><i class="icon-user"></i> <span>Admin User Manager</span></a>

        </li>


        <!-- /page kits -->

        </ul>
    </div>
</div>
<!-- /main navigation -->

