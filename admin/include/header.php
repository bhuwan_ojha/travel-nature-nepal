<?php
	require_once 'function.php';
	
	define("BASE_URL", base_url());
?>
<!DOCTYPE html>

<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible X-Frame-Options" content="IE=edge allow">

        <meta name="viewport" content="width=device-width, initial-scale=1">

       

        <title>Travel Nature Nepal - Admin Panel</title>



        <!-- Global stylesheets -->

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

        <link href="<?php echo BASE_URL;?>admin/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">

        <link href="<?php echo BASE_URL;?>admin/assets/css/bootstrap.css" rel="stylesheet" type="text/css">

        <link href="<?php echo BASE_URL;?>admin/assets/css/core.css" rel="stylesheet" type="text/css">

        <link href="<?php echo BASE_URL;?>admin/assets/css/components.css" rel="stylesheet" type="text/css">

        <link href="<?php echo BASE_URL;?>admin/assets/css/colors.css" rel="stylesheet" type="text/css">





        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/core/libraries/jquery.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/core/libraries/bootstrap.min.js"></script>



		
		<script src="<?php echo BASE_URL;?>admin/assets/ckeditor/ckeditor.js"></script>

		<script src="<?php echo BASE_URL;?>admin/assets/ckeditor/samples/js/sample.js"></script>

        <!-- Theme JS files -->

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/visualization/d3/d3.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/forms/styling/switchery.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/ui/moment/moment.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/pickers/daterangepicker.js"></script>

		<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/uploaders/fileinput.min.js"></script>

		<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/media/fancybox.min.js"></script>	

		<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/pages/gallery.js"></script>

		<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/pages/uploader_bootstrap.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/core/app.js"></script>

        <!--<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/pages/dashboard.js"></script>-->

		<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/plugins/uploaders/dropzone.min.js"></script>



	<script type="text/javascript" src="<?php echo BASE_URL;?>admin/assets/js/pages/uploader_dropzone.js"></script>

	  <!-- /theme JS files -->

      <script type="text/javascript">

          <?php require_once('menu.php'); ?>

          var slug = '<?php echo getSlug(); ?>';

          $(function () {

              $('[data-toggle="tooltip"]').tooltip()

              var menuObj = $("a[href="+slug+"]");

                if(menuObj.length) {
                    menuObj.closest("li").addClass("active");
                    if (menuObj.closest("li").parents("li").length){
                        menuObj.closest("li").parents("li").find("ul").show();
                        menuObj.closest("li").parents("li").addClass("active");
                    }
                }
            });

      </script>
    </head>

	

	