<?php


function is_ssl()
{
    if (isset($_SERVER['HTTPS'])) {
        if ('on' == strtolower($_SERVER['HTTPS']))
            return true;
        if ('1' == $_SERVER['HTTPS'])
            return true;
    } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
        return true;
    }
    return false;
}

function base_url()
{
$serverType = "http://";

if (is_ssl()) {
    $serverType = "https://";
}

return $serverType . $_SERVER['HTTP_HOST'].'/';

}
?>