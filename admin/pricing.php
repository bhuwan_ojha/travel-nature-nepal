<?php
session_start();
require_once('../Class/Connection.php');
require_once('../Class/Page.php');
if (isset($_SESSION['username'])) {

} else {
    header('location:login.php');
}
?>

<?php require_once('include/header.php'); ?>
<body>

    <!-- Main navbar -->
<?php require_once('include/nav-bar.php'); ?>

<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-main">
        <div class="sidebar-content">

            <!-- User menu -->
            <?php require_once('include/user_menu.php'); ?>
            <!-- /user menu -->


            <!-- Main navigation -->
            <?php require_once('include/side-nav-bar.php'); ?>

        </div>
    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ADD|UPDATE:</span> - Price Manager</h4>
                <button type="button"  style="margin-top: -15px" class="btn btn-success pull-right" onclick="window.location.href='edit_price.php'">View | Edit</button>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="form_inputs_basic.html">Forms</a></li>
                <li class="active">Price Manager</li>
            </ul>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <?php if (isset($_GET['success'])) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> Successfully Added.
                </div>
            <?php } ?>
            <?php if (isset($_GET['error'])) { ?>
                <div class="alert alert-danger">
                    <strong>Sorry!</strong> Unable to Add.
                </div>
            <?php } ?>
            <h5 class="panel-title">Add Places</h5>


            <br>
            <?php
            require_once '../Class/Price.php';
            require_once '../Class/Places.php';
            $id = isset($_GET['slug']) ? $_GET['slug'] : '';
            if ($id) {
                $objPlace = new Price();
                $data = $objPlace->getPriceById($id);
                if ($data != 0) {
                    foreach ($data as $value) {
                        ?>

                        <form action="pricing_process.php?id=<?php echo $value->id; ?>" enctype="multipart/form-data"
                              method="POST" role="form">
                            <div class="form-group">
                                <label class="control-label ">Place Name</label>
                                <select class="form-control" name="location"
                                        style="width: 180px;margin-top: -36px;margin-left: 100px;" id="sel1" required="">
                                    <option value="">Select a Destination</option>
                                    <?php
                                    $objPlace = new Places();
                                    $data = $objPlace->viewAllPlacesById();
                                    if ($data != 0) {
                                        foreach ($data as  $value) {
                                            ?>
                                            <option value="<?php echo $value['slug']; ?>"selected><?php echo $value['destination_name']; ?></option>

                                        <?php }} ?>
                                </select>

                            </div>
                            <?php
                            $objPlace = new Price();
                            $data = $objPlace->getPriceById($id);
                            if ($data != 0) {
                            foreach ($data as $value) {
                            ?>
                            <div class="form-group">
                                <label class="control-label "> Cost</label>
                                <input type="text" name="cost" class="form-control" value="<?php echo $value->cost; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label "> Price Description</label>
                                <textarea name="editor1" id="editor1" rows="10" cols="80">
                                    <?php echo $value->price_description; ?>
                                </textarea>
                                <script>
                                    // Replace the <textarea id="editor1"> with a CKEditor
                                    // instance, using default configuration.
                                    CKEDITOR.replace('editor1');
                                </script>
                            </div>
                                <div class="form-group">
                                    <label class="control-label "> Add Map URL</label>
                                    <input type="text" name="map" class="form-control" value='<?php echo $value->map; ?>'>
                                </div>
                                <div class="form-group">
                                    <label class="control-label "> Additional Information</label>
                                    <textarea name="editor2" id="editor2" rows="10" cols="80">
                                        <?php echo $value->additional_information; ?>
                                    </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace('editor2');
                                    </script>
                                </div>
                                <?php }} ?>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"> Save</button>
                            </div>

                        </form>
                    <?php }
                }
            } else { ?>
                <form action="pricing_process.php" enctype="multipart/form-data" method="POST" role="form">
                    <div class="form-group">
                        <label class="control-label ">Place Name</label>

                        <div>
                            <select class="form-control" name="location"
                                    style="width: 200px;margin-top: -36px;margin-left: 100px;" id="sel1">
                                <option value="">Select a Destination</option>
                                <?php
                                $objPlace = new Places();
                                $data = $objPlace->viewPlacesWithStatus();
                                print_r($data);
                                if ($data != 0) {
                                    foreach ($data as $key => $value) {
                                        ?>
                                        <option
                                            value="<?php echo $value->slug; ?>"><?php echo $value->destination_name; ?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label "> Cost</label>
                        <input type="text" name="cost" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label "> Price Description</label>
                        <textarea name="editor1" id="editor1" rows="10" cols="80">

                        </textarea>
                        <script>
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            CKEDITOR.replace('editor1');
                        </script>
                    </div>
                    <div class="form-group">
                        <label class="control-label ">Add Map URL</label>
                        <input type="text" name="map" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label "> Additional Information</label>
                        <textarea name="editor2" id="editor2" rows="10" cols="80">

                        </textarea>
                        <script>
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            CKEDITOR.replace('editor2');
                        </script>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> Save</button>
                    </div>

                </form>
            <?php } ?>

        </div>
    </div>


    <!-- /form horizontal -->


<?php require_once('include/footer.php'); ?>