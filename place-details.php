<?php include('includes/header.php'); ?>

<?php include('includes/nav.php'); ?>
<?php
require_once "Class/Connection.php";
require_once "Class/Places.php";

$objPlaces = new Places();
$data =$objPlaces->viewAllPlacesByIdIndex();

if($data!=0){
    foreach($data as $value){
        ?>
    <?php }} ?>

<div class="bread-bg-img">
    <div class="overlay-white-right">
        <div class="container">
            <div class="bread-bg-details">
                <h3 class="no-margin left-align grey-text text-darken-3"><?php echo $value['destination_name'];?></h3>
                <ul class="news-crumb">
                    <li><a href="#" class="grey-text text-darken-2">Home</a></li> \
                    <li><a href="#" class="grey-text text-darken-2">Destination</a></li> \
                    <li class="active red-text text-darken-4"><?php echo $value['destination_name'];?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="news-detail-sec white" style="padding: 30px 0;">
    <div class="container">
        <div class="row">
            <div class="col m8"><div class="col m11"><div class="row">
                        <?php
                        require_once "Class/Connection.php";
                        require_once "Class/Places.php";

                        $objPlaces = new Places();
                        $data =$objPlaces->viewAllPlacesByIdIndex();
                        if($data!=0){
                        foreach($data as $value){

                        ?>
                        <div class="main-news-heading left-align">
                            <h5 class="grey-text text-darken-3"><?php echo $value['destination_name'];?></h5><br>
                        </div>
                        <div class="img-holder-news">
                            <img src="<?php echo BASE_URL;?>uploads/Places/<?php echo $value['image'];?>"  width="60%" class="material-boxed responsive-img">
                        </div>
                        <div class="news-details-large">
                           <!-- <div class="date"><i class="fa fa-calendar"></i> Date: 2016-JUL-8</div>-->
                            <p><?php echo $value['description'];?></p>
                            <?php }} ?>
                            <br>
                            <div class="row no-margin grey lighten-4 z-depth-1">
                                <div class="col s12">
                                    <h5 class="section-heading grey-text text-darken-2">More Details</h5>
                                    <ul class="tabs">
                                        <li class="tab col s3"><a class="active" href="#test1">Gallery</a></li>
                                        <li class="tab col s3"><a href="#test2">Price Details</a></li>
                                        <li class="tab col s3"><a href="#test3">Information</a></li>
                                        <li class="tab col s3"><a href="#test4">Map</a></li>
                                    </ul>
                                </div>

                                <div id="test1" class="col s12 tab-inner">
                                    <div class="row no-margin">

                                        <?php
                                        $objImage = new Image();
                                        $data = $objImage->ViewAssignedImage();
                                        if ($data != 0) {
                                        foreach ($data as $key => $value) {
                                        ?>
                                        <div class="col 12">
                                            <img src="uploads/Banner/<?php echo $value->image; ?>" class="img-preview" width="180px" height="160px">
                                        </div>
                                        <?php }} ?>
                                    </div>
                                </div>
                                <div id="test2" class="col s12 tab-inner">
                                    <div class="row no-margin">
                                        <div class="col s12">
                                            <?php
                                            require_once "Class/Price.php";
                                            $objPrice = new Price();
                                            $data = $objPrice->getPriceById();
                                            if($data!=0){
                                                foreach($data as $value){
                                            ?>
                                            <h5>Cost: <span style="color: green">$<?php echo $value->cost;?></h5></span>
                                                    <br>
                                            <strong>Trip Cost Includes</strong>
                                            <br>
                                            <p><?php echo $value->price_description;?></p>
                                            <?php }} ?>
                                        </div>
                                    </div>
                                </div>
                                <div id="test3" class="col s12 tab-inner">
                                    <div class="row no-margin">
                                        <div class="col s12">
                                        <?php
                                        require_once "Class/Price.php";
                                        $objPrice = new Price();
                                        $data = $objPrice->getPriceById();
                                        if($data!=0){
                                            foreach($data as $value){
                                                ?>
                                                <h5>Additional Information</h5>

                                                <p><?php echo $value->additional_information;?></p>
                                            <?php }} ?>
                                            </div>

                                    </div>
                                </div>
                                <div id="test4" class="col s12 tab-inner">
                                    <div class="row no-margin">
                                        <div class="col l3">
                                            <?php
                                            require_once "Class/Price.php";
                                            $objPrice = new Price();
                                            $data = $objPrice->getPriceById();
                                            if($data!=0){
                                            foreach($data as $value){
                                            ?>
                                       <?php echo $value->map;?>

                                            <?php }} ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div></div>
            <!--<div class="col m4 grey lighten-4" style="border:1px solid #F5F5F5;padding: 9px 0 15px;margin-bottom: 15px;">
                <div class="col s12">
                    <div class="left-align">
                        <h5 class="section-heading grey-text text-darken-2">Top Stories</h5>
                    </div>
                </div>
                <div class="col s12">
                    <div class="news">
                        <div class="top-stories-box">
                            <h5><a href="#!" class="blue-text text-darken-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="time-news"><i class="fa fa-clock-o"></i> 2 hours ago</div>
                        </div>

                        <div class="border-gap"></div>

                        <div class="top-stories-box">
                            <h5><a href="#!" class="blue-text text-darken-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="time-news"><i class="fa fa-clock-o"></i> 3 hours ago</div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--<div class="col m4" style="border:1px solid #F5F5F5;background-color:#F9F9F9;padding: 9px 0 15px;margin-bottom: 15px;">
                <div class="col s12">
                    <div class="left-align">
                        <h5 class="section-heading grey-text text-darken-2">Special Offer</h5>
                    </div>
                </div>
                <div class="col s12">
                    <div class="news">
                        <div class="top-stories-box">
                            <h5><a href="#!" class="blue-text text-darken-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="time-news"><i class="fa fa-clock-o"></i> 2 hours ago</div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>
<?php include('includes/main-information.php');?>
<?php include('includes/recomended-destination.php');?>



<?php include('includes/footer.php');?>
