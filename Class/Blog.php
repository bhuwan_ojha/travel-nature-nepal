<?php



require_once 'Image.php';



class Blog extends Image {

	

	private $blog_title;

	private $category;

	private $image;

	private $description;

        private $video;

        

        function getVideo() {

            return $this->video;

        }



        function setVideo($video) {

            $this->video = $video;

        }



                        

        function getBlog_title() {

            return $this->blog_title;

        }



        function getCategory() {

            return $this->category;

        }



        function getImage() {

            return $this->image;

        }



        function getDescription() {

            return $this->description;

        }



        function setBlog_title($blog_title) {

            $this->blog_title = $blog_title;

        }



        function setCategory($category) {

            $this->category = $category;

        }



        function setImage($image) {

            $this->image = $image;

        }



        function setDescription($description) {

            $this->description = $description;

        }



                



        public function slugify($string){

            $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);

            return $slug;

        }





    public function addBlog($elementname) {

	$flag = $this->uploadImage ($elementname);

	$this->image = $this->getImage_link ();	

        $slug = $this->slugify($this->blog_title);

	$username = $_SESSION['username'];

	$this->sql = "INSERT INTO tbl_blog (slug,blog_title,category,image,video,description,datetime_created,added_by)VALUES('$slug','$this->blog_title','$this->category','$this->image','$this->video','$this->description', now() , '$username')";

        $this->res = mysqli_query ( $this->conxn, $this->sql ) or trigger_error ( $this->error = mysqli_error ( $this->conxn ) );

	$this->affRows = mysqli_affected_rows ( $this->conxn );

       

	if ($this->affRows > 0) {

	return TRUE;

	} else {

	return FALSE;

	}

}

	

	

	

	public function viewBlog(){

		$this->sql="SELECT * FROM tbl_blog order by id DESC ";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->numRows=mysqli_num_rows($this->res);

		if($this->numRows > 0){

			while($row=mysqli_fetch_object($this->res)){

				array_push($this->data,$row);

			}

			return $this->data;

		}

	}



	public function viewBlogIndex(){

		$this->sql="SELECT * FROM tbl_blog order by id DESC LIMIT 1";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->numRows=mysqli_num_rows($this->res);

		if($this->numRows > 0){

			while($row=mysqli_fetch_object($this->res)){

				array_push($this->data,$row);

			}

			return $this->data;

		}

	}

	

	

		public function viewBlogById(){

		$slug=isset($_GET['slug'])?$_GET['slug']:'';

		$this->sql="SELECT * FROM tbl_blog WHERE slug='$slug'";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->numRows=mysqli_num_rows($this->res);

		if($this->numRows > 0){

			while($row=mysqli_fetch_object($this->res)){

				array_push($this->data,$row);

			}

			return $this->data;

		}

	}

	

	public function viewAllBlogContent(){

		$this->sql="SELECT * FROM tbl_blog ";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->numRows=mysqli_num_rows($this->res);

		if($this->numRows > 0){

			while($row=mysqli_fetch_object($this->res)){

				array_push($this->data,$row);

			}

			return $this->data;

		}

	}

	

	

	public function viewBlogLimit(){

		$this->sql="SELECT * FROM tbl_blog ORDER BY  id  DESC LIMIT 8 ";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->numRows=mysqli_num_rows($this->res);

		if($this->numRows > 0){

			while($row=mysqli_fetch_object($this->res)){

				array_push($this->data,$row);

			}

			return $this->data;

		}

	}

	

	

	public function updateBlog($elementname){

		$slug=isset($_GET['slug'])?$_GET['slug']:'';

		$flag = $this->uploadImage ( $elementname );

		$this->image = $this->getImage_link ();

		if ($this->image != 0) {

			$this->sql = "UPDATE tbl_blog SET blog_title='$this->blog_title',category='$this->category',image='$this->image',video='$this->video',description='$this->description' WHERE slug='$slug'";

                }else{

                    $this->sql = "UPDATE tbl_blog SET blog_title='$this->blog_title',category='$this->category',video='$this->video',description='$this->description' WHERE slug='$slug'";

                }

			$this->res = mysqli_query ( $this->conxn, $this->sql ) or trigger_error ( $this->error = mysqli_error ( $this->conxn ) );

			$this->affRows = mysqli_affected_rows ( $this->conxn );

			if ($this->affRows > 0) {

				return TRUE;

			} else {

				return FALSE;

			}

		}

	



	

		public function deleteBlog(){

		$slug=$_GET['slug'];

		$this->sql="DELETE FROM tbl_blog WHERE slug='$slug'";

		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));

		$this->affRows=mysqli_affected_rows($this->conxn);

		if($this->affRows > 0){

                @unlink('../uploads/' . $this->image);

			return TRUE;

		}

		else{

			return FALSE;

		}

	}

	



		

		

	}

	

	





