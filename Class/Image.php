<?php



class Image extends Connection
{


    private $image_link;

    private $image_date;

    private $banner_image;

    private $title;


    /**
     * @return the $title
     */

    public function getTitle()
    {

        return $this->title;

    }


    /**
     * @param field_type $title
     */

    public function setTitle($title)
    {

        $this->title = $title;

    }


    /**
     * @return the $banner_image
     */

    public function getBanner_image()
    {

        return $this->banner_image;

    }


    /**
     * @param Ambigous <the, string> $banner_image
     */

    public function setBanner_image($banner_image)
    {

        $this->banner_image = $banner_image;

    }


    /**
     * @return the $image_id
     */

    public function getImage_id()
    {

        return $this->image_id;

    }


    /**
     * @return the $image_link
     */

    public function getImage_link()
    {

        return $this->image_link;

    }


    /**
     * @return the $image_date
     */

    public function getImage_date()
    {

        return $this->image_date;

    }


    /**
     * @param field_type $image_id
     */

    public function setImage_id($image_id)
    {

        $this->image_id = $image_id;

    }


    /**
     * @param string $image_link
     */

    public function setImage_link($image_link)
    {

        $this->image_link = $image_link;

    }


    /**
     * @param field_type $image_date
     */

    public function setImage_date($image_date)
    {

        $this->image_date = $image_date;

    }


    public function __construct()
    {

        parent:: __construct();

    }


    public function uploadImage($elementname)
    {

        $filename = $_FILES[$elementname]['name'];

        $newName = date('y_m_d_h_i_s_') . rand(0, 1500) . $filename;

        $tmp_name = $_FILES[$elementname]['tmp_name'];
        if ($elementname == 'banner_image') {
            $destination = "../uploads/Banner/" . $newName;
        } elseif ($elementname == 'places') {
            $destination = "../uploads/Places/" . $newName;
        } elseif ($elementname == 'image_file') {
            $destination = "../uploads/PageImage/" . $newName;
        } else {
            $destination = "../uploads/" . $newName;
        }

        if (move_uploaded_file($tmp_name, $destination)) {

            $this->image_link = $newName;

            return TRUE;

        } else {

            return FALSE;

        }

    }


    public function addImage($elementname)
    {

        $flag = $this->uploadImage($elementname);

        $this->image = $this->getImage_link();

        if ($flag == TRUE) {

            $this->sql = "INSERT INTO tbl_page(image,image_added)

            values('$this->image',now())";

            $this->res = mysqli_query($this->conxn, $this->sql)

            or trigger_error($this->error = mysqli_error($this->conxn));

            $this->affRows = mysqli_affected_rows($this->conxn);

            if ($this->affRows > 0) {

                return TRUE;

            } else {

                //remove the file from the server

                @unlink('../uploads/' . $this->image_link);

                return FALSE;

            }

        } else {

            return FALSE;

        }

    }


    public function viewImage()
    {

        $this->sql = "SELECT * FROM tbl_pages ";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

    }


    public function bannerImage($elementname)
    {

        $flag = $this->uploadImage($elementname);

        $this->banner_image = $this->getImage_link();

        if ($flag == TRUE) {
            $this->sql = "INSERT INTO tbl_banner(title,image,image_added_date)

            values('$this->title','$this->banner_image',now())";


            $this->res = mysqli_query($this->conxn, $this->sql)

            or trigger_error($this->error = mysqli_error($this->conxn));

            $this->affRows = mysqli_affected_rows($this->conxn);

            if ($this->affRows > 0) {

                return TRUE;

            } else {
                //remove the file from the server
                @unlink('../uploads/Banner/' . $this->image_link);

                return FALSE;

            }

        } else {

            return FALSE;

        }

    }


    public function viewBannerImage()
    {

        $this->sql = "SELECT * FROM tbl_banner";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

    }


    public function viewBannerInIndex()
    {

        $this->sql = "SELECT * FROM tbl_banner WHERE status = 1";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

    }


    public function deleteBannerImage()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $this->sql = "DELETE FROM tbl_banner WHERE id='$id'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->affRows = mysqli_affected_rows($this->conxn);
        if ($this->affRows > 0) {
            return TRUE;
        } else {
            return FALSE;

        }

    }


    public function getBannerStatus()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : '';

        $this->sql = "SELECT status FROM tbl_banner WHERE id='$id'";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

    }


    public function updateBannerStatus()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : '';

        $status = isset($_GET['status']) ? $_GET['status'] : '';

        if ($status == 1) {

            $this->sql = "UPDATE tbl_banner SET status = '0' WHERE id='$id'";

        } else {

            $this->sql = "UPDATE tbl_banner SET status='1' WHERE id='$id'";


        }

        $this->res = mysqli_query($this->conxn, $this->sql)

        or trigger_error($this->error = mysqli_error($this->conxn));

        $this->affRows = mysqli_affected_rows($this->conxn);

        if ($this->affRows > 0) {

            return TRUE;

        } else {


        }

    }


    public function viewGallery()
    {

        $this->sql = "SELECT * FROM tbl_banner WHERE status=0";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

    }


    public function deleteBanner()
    {

        $id = $_GET['id'];
        $this->sql = "SELECT image FROM tbl_banner WHERE id='$id'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            $image = $this->data[0];
            $image = $image->image;
            unlink("../uploads/Banner/$image");

            $this->sql = "DELETE FROM tbl_banner WHERE image='$image'";

            $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

            $this->affRows = mysqli_affected_rows($this->conxn);

            if ($this->affRows > 0) {

                return TRUE;

            } else {

                return FALSE;

            }


        }
    }

    public function assignImageToLocation($image,$location){
        $location = base64_encode($location);
        $this->sql = "INSERT INTO tbl_assignee(image,location,created_date)values('$image','$location',now())";

        $this->res = mysqli_query($this->conxn, $this->sql)

        or trigger_error($this->error = mysqli_error($this->conxn));

        $this->affRows = mysqli_affected_rows($this->conxn);

        if ($this->affRows > 0) {

            return TRUE;

        } else {

            return FALSE;

        }
    }


    public function ViewAssignedImage(){
        $location = $_GET['slug'];
        $this->sql = "SELECT * FROM tbl_assignee WHERE location= '$location' ORDER BY RAND() LIMIT 4";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));

        $this->numRows = mysqli_num_rows($this->res);

        if ($this->numRows > 0) {

            while ($row = mysqli_fetch_object($this->res)) {

                array_push($this->data, $row);

            }

            return $this->data;

        }

        }







}