<?php

Class Seo extends Connection{
	private $page_name;
	private $link;
	private $title;
	private $description;
	private $keywords;
	private $language;
	private $robots;
	private $copyright;
	private $revisit;
	private $topic;
	private $audience;
	private $expires; 
	private $placename; 
	private $region;
	private $position;
	private $google_analytics;
	private $google_site_verification;
	
	
	
	/**
	 * @return the $language
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @return the $robots
	 */
	public function getRobots() {
		return $this->robots;
	}

	/**
	 * @return the $copyright
	 */
	public function getCopyright() {
		return $this->copyright;
	}

	/**
	 * @return the $revisit
	 */
	public function getRevisit() {
		return $this->revisit;
	}

	/**
	 * @return the $topic
	 */
	public function getTopic() {
		return $this->topic;
	}

	/**
	 * @return the $audience
	 */
	public function getAudience() {
		return $this->audience;
	}

	/**
	 * @return the $expires
	 */
	public function getExpires() {
		return $this->expires;
	}

	/**
	 * @return the $placename
	 */
	public function getPlacename() {
		return $this->placename;
	}

	/**
	 * @return the $region
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * @return the $position
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * @return the $google_analytics
	 */
	public function getGoogle_analytics() {
		return $this->google_analytics;
	}

	/**
	 * @return the $google_site_verification
	 */
	public function getGoogle_site_verification() {
		return $this->google_site_verification;
	}

	/**
	 * @param field_type $language
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}

	/**
	 * @param field_type $robots
	 */
	public function setRobots($robots) {
		$this->robots = $robots;
	}

	/**
	 * @param field_type $copyright
	 */
	public function setCopyright($copyright) {
		$this->copyright = $copyright;
	}

	/**
	 * @param field_type $revisit
	 */
	public function setRevisit($revisit) {
		$this->revisit = $revisit;
	}

	/**
	 * @param field_type $topic
	 */
	public function setTopic($topic) {
		$this->topic = $topic;
	}

	/**
	 * @param field_type $audience
	 */
	public function setAudience($audience) {
		$this->audience = $audience;
	}

	/**
	 * @param field_type $expires
	 */
	public function setExpires($expires) {
		$this->expires = $expires;
	}

	/**
	 * @param field_type $placename
	 */
	public function setPlacename($placename) {
		$this->placename = $placename;
	}

	/**
	 * @param field_type $region
	 */
	public function setRegion($region) {
		$this->region = $region;
	}

	/**
	 * @param field_type $position
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * @param field_type $google_analytics
	 */
	public function setGoogle_analytics($google_analytics) {
		$this->google_analytics = $google_analytics;
	}

	/**
	 * @param field_type $google_site_verification
	 */
	public function setGoogle_site_verification($google_site_verification) {
		$this->google_site_verification = $google_site_verification;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return the $keywords
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * @param field_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @param field_type $keywords
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * @return the $page_name
	 */
	public function getPage_name() {
		return $this->page_name;
	}

	/**
	 * @return the $link
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * @param field_type $page_name
	 */
	public function setPage_name($page_name) {
		$this->page_name = $page_name;
	}

	/**
	 * @param field_type $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}

	public function __construct(){
		parent::__construct();
	}
        
        public function slugify($string){
            $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
            return $slug;
        }
        
        public function addSeo(){
                $slug = $this->slugify($this->link);
              
		$this->sql="INSERT INTO tbl_seo (name , title , description , keywords , link)VALUES('$this->page_name','$this->title','$this->description','$this->keywords','$slug')";
		
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	public function ViewSeo(){
		$this->sql="SELECT * FROM tbl_seo";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->numRows=mysqli_num_rows($this->res);
		if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
				array_push($this->data,$row);
			}
			return $this->data;
		}
	}
	
        public function ViewSeoIndex($title){
            
		$this->sql="SELECT * FROM tbl_seo WHERE title = '$title'";
                
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->numRows=mysqli_num_rows($this->res);
		if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
				array_push($this->data,$row);
			}
			return $this->data;
		}
	}
	
	public function ViewSeoContents(){
		$id=isset($_GET['id'])?$_GET['id']:'';
		$this->sql="SELECT * FROM tbl_seo WHERE id='$id'";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->numRows=mysqli_num_rows($this->res);
		if($this->numRows > 0){
		while($row=mysqli_fetch_object($this->res)){
		array_push($this->data,$row);
		}
		return $this->data;
		}
		}
	
	
	
	public function UpdateSeo(){
		$id=isset($_GET['id'])?$_GET['id']:'';
		$this->sql="UPDATE tbl_seo SET name='$this->page_name' , title='$this->title' , description='$this->description' , keywords='$this->keywords' , link='$this->link' WHERE id='$id'";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function DeleteSeo(){
		$id=$_GET['id'];
		$this->sql="DELETE FROM tbl_seo WHERE id='$id'";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function addCommonSeoMeta(){
		$this->sql="INSERT INTO tbl_seo_common (language, robots, copyright, revisit, topic, audience, expires, placename, region, position, google_analytics, google_site_verification)
	 		   VALUES ('$this->language','$this->robots','$this->copyright','$this->revisit','$this->topic','$this->audience','$this->expires','$this->placename', '$this->region','$this->position','$this->google_analytics','$this->google_site_verification')";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function updateCommonSeoMeta(){
		$this->sql="UPDATE tbl_seo_common SET language='$this->language', robots='$this->robots', copyright='$this->copyright', revisit='$this->revisit', topic='$this->topic', audience='$this->audience', expires='$this->expires', placename='$this->placename', region='$this->region', position='$this->position', google_analytics='$this->google_analytics', google_site_verification='$this->google_site_verification'";
		
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	
	
	public function viewCommonSeoMetaContents(){
		$id=isset($_GET['id'])?$_GET['id']:'';
		$this->sql="SELECT * FROM tbl_seo_common";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->numRows=mysqli_num_rows($this->res);
		if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
				array_push($this->data,$row);
			}
			return $this->data;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}