<?php

require_once 'Image.php';

class Places extends Image
{

    private $place_name;
    private $is_what;
    private $places;
    private $editor1;

    /**
     *
     * @return the $place_name
     */
    public function getPlace_name()
    {
        return $this->place_name;
    }

    /**
     *
     * @return the $is_what
     */
    public function getIs_what()
    {
        return $this->is_what;
    }

    /**
     *
     * @return the $places
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     *
     * @param $place_name field_type
     */
    public function setPlace_name($place_name)
    {
        $this->place_name = $place_name;
    }

    /**
     *
     * @param $is_what field_type
     */
    public function setIs_what($is_what)
    {
        $this->is_what = $is_what;
    }

    /**
     *
     * @param $places field_type
     */
    public function setPlaces($places)
    {
        $this->places = $places;
    }

    /**
     *
     * @return the $editor1
     */
    public function getEditor1()
    {
        return $this->editor1;
    }

    /**
     *
     * @param $editor1 field_type
     */
    public function setEditor1($editor1)
    {
        $this->editor1 = $editor1;
    }

    public function slugify($string)
    {

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);

        return $slug;

    }

    public function addPlaces($elementname)
    {
        $flag = $this->uploadImage($elementname);
        $this->places = $this->getImage_link();
        $slug = $this->slugify($this->place_name);
        if ($this->places != 0) {
            $this->sql = "INSERT INTO tbl_destination (destination_name,category,image,description,slug)VALUES('$this->place_name','$this->is_what','$this->places','$this->editor1' , '$slug')";
            $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
            $this->affRows = mysqli_affected_rows($this->conxn);
            if ($this->affRows > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }


    public function viewAllPlaces()
    {
        $this->sql = "SELECT * FROM tbl_destination ORDER BY RAND() DESC ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function viewPlacesWithStatus()
    {
        $this->sql = "SELECT * FROM tbl_destination WHERE status = '0'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }


    public function viewAllPlacesSideList()
    {
        $this->sql = "SELECT * FROM tbl_destination order by id ASC LIMIT 17 ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }

    }

    public function viewAllPlacesLimit()
    {
        $this->sql = "SELECT * FROM tbl_destination order by id DESC limit 4  ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

        public function viewAllPlacesById()
    {
        $slug = basename($_SERVER['REQUEST_URI']);
      
        $this->sql = "SELECT * FROM tbl_destination WHERE slug='$slug' order by id ASC ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_assoc($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }
    
    public function viewAllPlacesByIdIndex()
    {
        $slug = basename($_SERVER['REQUEST_URI']);
        $slug = base64_decode($slug);
        $this->sql = "SELECT * FROM tbl_destination WHERE slug='$slug' order by id ASC ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_assoc($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function viewAllPlacesByCategory()
    {
        
        $category = urldecode(basename($_SERVER['REQUEST_URI']));

        $this->sql = "SELECT * FROM tbl_destination WHERE category='$category' order by id ASC ";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_assoc($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function getSelectedPlaceName()
    {
        $id = basename($_SERVER['REQUEST_URI']);
        $slug = base64_decode($id);
        $this->sql = "SELECT destination_name FROM tbl_destination WHERE slug='$slug'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_assoc($this->res)) {
                array_push($this->data, $row);
            }
            $data = $this->data;
            $data = $data[0]['destination_name'];
            return $data;


        }
    }


    public function viewRecommendedPlaces()
    {
        $this->sql = "SELECT * FROM tbl_destination WHERE category='1' order by id DESC";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function viewTrendingPlaces()
    {
        $this->sql = "SELECT * FROM tbl_destination   ORDER BY RAND() Limit 8";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function viewRecommendedPlacesIndex()
    {
        $this->sql = "SELECT * FROM tbl_destination   ORDER BY RAND()  DESC LIMIT 5";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }


    public function viewTopPlaces()
    {
        $this->sql = "SELECT * FROM tbl_destination WHERE category='2' order by id DESC";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function viewTopPlacesIndex()
    {
        $this->sql = "SELECT * FROM tbl_destination ORDER BY RAND()  ASC LIMIT 4";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function updatePlaces($elementname)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $flag = $this->uploadImage($elementname);
        $slug = $this->slugify($this->place_name);
        $this->places = $this->getImage_link();
        if ($this->places != 0) {
            $this->sql = "UPDATE tbl_destination SET destination_name='$this->place_name',category='$this->is_what',image='$this->places',description='$this->editor1' , slug ='$slug' WHERE id='$id'";
            print_r($this->sql);
            $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
            $this->affRows = mysqli_affected_rows($this->conxn);
            if ($this->affRows > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function updatePlacesWithoutImage()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $slug = $this->slugify($this->place_name);
        $this->sql = "UPDATE tbl_destination SET destination_name='$this->place_name',category='$this->is_what',description='$this->editor1',slug ='$slug' WHERE id='$id'";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->affRows = mysqli_affected_rows($this->conxn);
        if ($this->affRows > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function deletePlaces()
    {
        $id = $_GET['id'];
        $this->sql = "SELECT image FROM tbl_destination WHERE id='$id'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            $image = $this->data[0];
            $image = $image->image;
            unlink("../uploads/Places/$image");
            $this->sql = "DELETE FROM tbl_destination WHERE image='$image'";
            $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
            $this->affRows = mysqli_affected_rows($this->conxn);


            return $this->affRows > 0;

        }
    }

    public function viewSelectedPlace($location_slug)
    {

        $location_slug = base64_decode($location_slug);

        $this->sql = "SELECT * FROM tbl_destination WHERE slug='$location_slug' DESC ";

        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }
    }

    public function getCategory(){
        $this->sql = "SELECT DISTINCT (category) FROM tbl_destination";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }
            return $this->data;
        }

    }

    public function getCategoryById(){
        $slug = base64_decode($_GET['slug']);
        $this->sql = "SELECT DISTINCT (category) FROM tbl_destination WHERE slug = '$slug'";
        $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
        $this->numRows = mysqli_num_rows($this->res);
        if ($this->numRows > 0) {
            while ($row = mysqli_fetch_object($this->res)) {
                array_push($this->data, $row);
            }

            return $this->data;
        }

    }


}
	
	


