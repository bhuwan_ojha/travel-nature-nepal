<?php
require_once 'Image.php';
Class Page extends Image{
	private $page_name;
	private $editor1;
	private $image_file;
	
	

	/**
	 * @return the $image_file
	 */
	public function getImage_file() {
		return $this->image_file;
	}

	/**
	 * @param field_type $image_file
	 */
	public function setImage_file($image_file) {
		$this->image_file = $image_file;
	}

	/**
	 * @return the $page_name
	 */
	public function getPage_name() {
		return $this->page_name;
	}

	/**
	 * @return the $editor1
	 */
	public function getEditor1() {
		return $this->editor1;
	}

	/**
	 * @param field_type $page_name
	 */
	public function setPage_name($page_name) {
		$this->page_name = $page_name;
	}

	/**
	 * @param field_type $editor1
	 */
	public function setEditor1($editor1) {
		$this->editor1 = $editor1;
	}

	public function __construct(){
		parent:: __construct();
	}
	
	public function AddPage(){
		$this->sql="INSERT INTO tbl_pages(page_name,page_desc)VALUES('$this->page_name','$this->editor1')";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function DeletePage(){
		$id=$_GET['id'];
		$this->sql="DELETE FROM tbl_pages WHERE id='$id'";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function PageList(){
		$this->sql="SELECT * FROM tbl_pages";		
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->numRows=mysqli_num_rows($this->res);		
		if($this->numRows > 0){
		while($row=mysqli_fetch_object($this->res)){
		array_push($this->data,$row);	
			}
		return $this->data;
		}
	}
	
	public function ViewPageContents(){
			$id=isset($_GET['id'])?$_GET['id']:'';
			$this->sql="SELECT * FROM tbl_pages WHERE id='$id'";		
			$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
			$this->numRows=mysqli_num_rows($this->res);		
			if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
			array_push($this->data,$row);	
				}
			return $this->data;
			}
		}
		
		
		public function ViewAboutContents(){
			$id=isset($_GET['id'])?$_GET['id']:'';
			$this->sql="SELECT * FROM tbl_pages WHERE id='1'";
			$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
			$this->numRows=mysqli_num_rows($this->res);
			if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
			array_push($this->data,$row);
			}
			return $this->data;
			}
			}

	
	
	public function EditPageDetails($elementname){

        try {
            $id = $_GET['id'];
            $flag = $this->uploadImage($elementname);
            $this->image_file = $this->getImage_link();
            if ($this->image_file != 0) {
                $this->sql = "SELECT image FROM tbl_pages WHERE id='$id'";
                $this->res = mysqli_query($this->conxn, $this->sql) or trigger_error($this->error = mysqli_error($this->conxn));
                $this->numRows = mysqli_num_rows($this->res);

                if ($this->numRows > 0) {
                    while ($row = mysqli_fetch_object($this->res)) {
                        array_push($this->data, $row);
                        unlink('../uploads/PageImage/' . $row->image);
                    }

                }

                $this->sql = "UPDATE tbl_pages SET page_desc='$this->editor1' , page_name='$this->page_name' , image='$this->image_file' , image_added=now()   WHERE id='$id'";
            } else {
                $this->sql = "UPDATE tbl_pages SET page_desc='$this->editor1' , page_name='$this->page_name' WHERE id='$id'";
            }
            $this->res = mysqli_query($this->conxn, $this->sql)
            or trigger_error($this->error = mysqli_error($this->conxn));
            $this->affRows = mysqli_affected_rows($this->conxn);

            return TRUE;
        }catch (Exception $e){
            return FALSE;
        }


	}
	
	
	
	
}