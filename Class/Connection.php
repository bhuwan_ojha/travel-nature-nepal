<?php
class Connection{
    private $host;
    private $user;
    private $pwd;
    private $db;
    
    public $conxn;
    public $sql;
    public $data = array();
    public $res;
    public $numRows;
    public $affRows;
    public $error;
    
    public function __construct($host = 'localhost', $u = 'travelna_travel', $p = 'g(qD$^iC%g~9', $db = 'travelna_travel_nepal'){
    
        //set the properties
        $this->host = $host;
        $this->user = $u;
        $this->pwd = $p;
        $this->db = $db;
        
        //connect to the database
        $this->conxn = mysqli_connect($this->host, $this->user  , $this->pwd, $this->db)
                        or trigger_error($this->error = mysqli_error($this->conxn));
        
        
    }
    
    //destructor
    public function __destruct(){
        
       
    mysqli_close($this->conxn);
    
    }
}
    