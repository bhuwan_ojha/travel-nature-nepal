<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/6/16
 * Time: 9:30 PM
 */
class Price extends Connection{

    public function slugify($string){

        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);

        return $slug;

    }


    function addPrice($cost,$location_slug,$price_description,$additional_information,$map){
        $location_slug = $this->slugify($location_slug);
        $this->sql = "INSERT INTO tbl_price(cost,location_slug,price_description,additional_information,map)VALUES('$cost','$location_slug','$price_description','$additional_information','$map')";

        $this->res = mysqli_query($this->conxn,$this->sql) or trigger_error($this->error($this->conxn));
        $this->affRows = mysqli_affected_rows($this->conxn);
        if($this->affRows > 0){
            $this->sql1 = "UPDATE  tbl_destination SET `status` = 1 WHERE slug = '$location_slug'";
            print_r($this->sql1);
            $this->res = mysqli_query($this->conxn,$this->sql1) or trigger_error($this->error($this->conxn));
            $this->affRows = mysqli_affected_rows($this->conxn);
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function getPrice(){
        $this->sql = "SELECT * FROM tbl_price JOIN tbl_destination ON tbl_price.location_slug = tbl_destination.slug";
        $this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
        $this->numRows=mysqli_num_rows($this->res);
        if($this->numRows > 0){
            while($row=mysqli_fetch_object($this->res)){
                array_push($this->data,$row);

            }
            return $this->data;
        }
    }

    public function getPriceById(){
        $location_slug = isset($_GET['slug'])?$_GET['slug']:'';
        $location_slug = base64_decode($location_slug);
        $this->sql = "SELECT * FROM tbl_price WHERE location_slug = '$location_slug'";
        $this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
        $this->numRows=mysqli_num_rows($this->res);
        if($this->numRows > 0){
            while($row=mysqli_fetch_object($this->res)){
                array_push($this->data,$row);

            }
            return $this->data;
        }
    }

    public function viewSelectedPlace($location_slug){

        $location_slug = base64_decode($location_slug);

        $this->sql="SELECT * FROM tbl_destination WHERE slug='$location_slug' DESC ";
        print_r($this->sql);
        $this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
        $this->numRows=mysqli_num_rows($this->res);
        if($this->numRows > 0){
            while($row=mysqli_fetch_object($this->res)){
                array_push($this->data,$row);
            }
            return $this->data;
        }
    }

    public function updatePrice($cost,$price_description,$additional_information,$map){
        $id=isset($_GET['id'])?$_GET['id']:'';
        $this->sql = "UPDATE tbl_price SET cost='$cost',price_description='$price_description',additional_information='$additional_information',map = '$map' WHERE id='$id'";

        $this->res = mysqli_query ( $this->conxn, $this->sql ) or trigger_error ( $this->error = mysqli_error ( $this->conxn ) );
        $this->affRows = mysqli_affected_rows ( $this->conxn );
        if ($this->affRows > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
}

}