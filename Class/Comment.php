<?php
Class Comment extends Connection{
	private $name;
	private $address;
	private $email;
	private $comment;
        function getName() {
            return $this->name;
        }

        function getAddress() {
            return $this->address;
        }

        function getEmail() {
            return $this->email;
        }

        function getComment() {
            return $this->comment;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setAddress($address) {
            $this->address = $address;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setComment($comment) {
            $this->comment = $comment;
        }

        

	
	public function __construct(){
		parent:: __construct();
	}
	
	
	public function AddComment($commented_to){
		$this->sql="INSERT INTO tbl_comment(name,email,comment,commented_date,commented_to)VALUES('$this->name','$this->email','$this->comment',now() , '$commented_to')";
		$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
		$this->affRows=mysqli_affected_rows($this->conxn);
		if($this->affRows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
		
	}
	
	
	public function ViewComment(){
			$id= $_SESSION['id'];
			$this->sql="SELECT name,comment,commented_date,tbl_comment.id , blog_title,email FROM tbl_comment JOIN tbl_blog WHERE tbl_comment.commented_to = tbl_blog.slug AND status = '1'";
			$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
			$this->numRows=mysqli_num_rows($this->res);
			if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
			array_push($this->data,$row);
			}			
		
			return $this->data;


			}
			}


                        
        public function ViewCommentApproved(){		
                        $slug=isset($_GET['slug'])?$_GET['slug']:'';
			$this->sql="SELECT * FROM tbl_comment WHERE status = '0' AND commented_to='$slug' ORDER BY id DESC";
			$this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
			$this->numRows=mysqli_num_rows($this->res);
			if($this->numRows > 0){
			while($row=mysqli_fetch_object($this->res)){
			array_push($this->data,$row);
			}			
		
			return $this->data;
			
			
			
			}
			}
	
	
	public function CountComment(){
            $this->sql="SELECT * FROM tbl_comment WHERE status='1'";
            $this->res=mysqli_query($this->conxn , $this->sql) or trigger_error($this->error=mysqli_error($this->conxn));
            $this->numRows=mysqli_num_rows($this->res);
            if($this->numRows > 0)
            {
            while($row=mysqli_fetch_assoc($this->res)){
            array_push($this->data , $row);
            }
            return $this->numRows;
            }
	}
				  
				  
    public function UpdateCommentStatus(){
        $id=isset($_GET['id'])?$_GET['id']:'';				  		
	$this->sql="UPDATE tbl_comment SET status='0' WHERE id='$id'";
	$this->res=mysqli_query($this->conxn , $this->sql) or trigger_error($this->error=mysqli_error($this->conxn));
	$this->affRows=mysqli_affected_rows($this->conxn);
	if($this->affRows > 0){				  		
            $this->sql="SELECT * FROM `tbl_comment` WHERE status='1' ";
            $this->res=mysqli_query($this->conxn , $this->sql) or trigger_error($this->error=mysqli_error($this->conxn));
            $this->numRows=  mysqli_num_rows($this->res);                                        
            return $this->numRows;
            }				  	                                      
	else{
	return FALSE;
	}
    }



    public function replyComment($reply_to,$content){
        $this->sql="INSERT INTO tbl_reply(reply_to,content,replied_date)VALUES('$reply_to','$content',now())";
        $this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
        $this->affRows=mysqli_affected_rows($this->conxn);
        if($this->affRows > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }

    }


    public function ViewReply($id){
        $this->sql="SELECT * FROM tbl_reply   WHERE reply_to = $id ";
        $this->res=mysqli_query($this->conxn,$this->sql)or trigger_error($this->error=mysqli_error($this->conxn));
        $this->numRows=mysqli_num_rows($this->res);
        if($this->numRows > 0){
            while($row=mysqli_fetch_object($this->res)){
                array_push($this->data,$row);
            }

            return $this->data;



        }
    }

    public function RemoveComment(){
        $id=isset($_GET['id'])?$_GET['id']:'';
        $this->sql="UPDATE tbl_comment SET status='2' WHERE id='$id'";
        $this->res=mysqli_query($this->conxn , $this->sql) or trigger_error($this->error=mysqli_error($this->conxn));
        $this->affRows=mysqli_affected_rows($this->conxn);
        if($this->affRows > 0){
            $this->sql="SELECT * FROM `tbl_comment` WHERE status='1' ";
            $this->res=mysqli_query($this->conxn , $this->sql) or trigger_error($this->error=mysqli_error($this->conxn));
            $this->numRows=  mysqli_num_rows($this->res);
            return $this->numRows;
        }
        else{
            return FALSE;
        }
    }


















}