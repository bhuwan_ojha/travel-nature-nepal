<?php include('includes/header.php');?>

    <?php include('includes/nav.php');?>

    <div class="bread-bg-img">
      <div class="overlay-white-right">
        <div class="container">
          <div class="bread-bg-details">
            <h3 class="no-margin left-align grey-text text-darken-3">Contact Us</h3>

          </div>
        </div>
      </div>
    </div>

    <section class="news-detail-sec white" style="padding: 30px 0;">
        <div class="container">
          <div class="row">
            <div class="col m8 s12">
              <div class="col m11 s12">
                <div class="row">
                <form action="#" method="POST" id="contact-form">
                <h5 class="section-heading grey-text text-darken-2">Send us message</h5><br>
                    <div class="row">
                      <div class="input-field col s6">
                        <input placeholder="Your Name" id="name" name="name" type="text" class="">
                        <label for="name">Name:</label>
                          <span id="name-error" style="display: none">This field is required</span>
                      </div>

                      <div class="input-field col s6">
                        <input placeholder="Your email" id="email" name="email" type="email" class="">
                        <label for="name">Email:</label>
                          <span id="email-error" style="display: none">This field is required</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input placeholder="Your Address" name="address" id="address" type="text" class="">
                        <label for="address">Address:</label>
                          <span id="address-error" style="display: none">This field is required</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s6">
                        <input placeholder="Your Number" name="phone" id="phone" type="tel" maxlength="10" class="">
                        <label for="phone">Phone Number:</label>
                          <span id="phone-error"  style="display: none">This field is required</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="message" name="description" placeholder="your message here" class="materialize-textarea "></textarea>
                        <label for="textarea1">Description:</label>
                          <span id="message-error" style="display: none">This field is required</span>
                      </div>
                    </div>
                    <button class="btn-large red darken-4 waves-effect waves-light" type="submit" name="action">Submit
                      <i class="material-icons right">send</i>
                    </button><br><br>
                    <div id="contact-thankyou" style="color: green"></div>
                    <div id="contact-error" style="color: red"></div>
                </form>
                </div>
              </div>
            </div>


            <div class="col m4 s12 grey lighten-4" style="border:1px solid #F5F5F5;padding: 9px 0 15px;margin-bottom: 15px;">
              <div class="col s12">
                <div class="left-align">
                  <h5 class="section-heading grey-text text-darken-2">Map</h5>

                </div>
              </div>
              <div class="col s12">
                <div class="news">
                  <div class="top-stories-box">
                    <h5><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.9384234287304!2d85.33195431558703!3d27.68829798279981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19be007dba3d%3A0x75c73e40ae387525!2sHimalayan+Kingdom+Tours+%26+Travels+P.Ltd.!5e0!3m2!1sen!2snp!4v1481522722960" width="100%" height="450" frameborder="0" style="border:0" ></iframe></h5>

                    </div>
                  </div>
                </div>
              </div>
              <div class="col m4 s12" style="border:1px solid #F5F5F5;background-color:#F9F9F9;padding: 9px 0 15px;margin-bottom: 15px;">
                <div class="col s12">
                  <div class="left-align">
                    <h5 class="section-heading grey-text text-darken-2">Our location</h5>
                  </div>
                </div>
                <div class="col s12">
                  <div class="news">
                    <div class="top-stories-box">
                      <ul>
                        <li class="grey-text text-darken-3">Travel Nature Nepal Tours & Travels</li>
                        <li class="grey-text text-darken-3">Kathmandu, Nepal</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-phone"></i> (123)456789</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-inbox"></i> info@travelnaturenepal.com</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>  
          </div>
        </div>
      </section>
	  <?php include('includes/main-information.php');?>

<script>
    $(function(){
        $('#contact-form').on("submit", function(e){
            e.preventDefault();

            var name = $('#name').val();
            var email = $('#email').val();
            var address = $('#address').val();
            var phone = $('#phone').val();
            var message = $('#message').val();
            if (name == '') {
                $('#name').css("border-color","red");
            } else {
                $('#name').css("border-color","#26A69A");
                $('name-error').show();
            }
            if(address==''){
                $('#address').css("border-color","red");
            } else {
                $('#address').css("border-color","#26A69A");
            }
            if(phone==''){
                $('#phone').css("border-color","red");
            } else {
                $('#phone').css("border-color","#26A69A");
            }
            if(message=='') {
                $('#message').css("border-color","red");
            } else {
                $('#message').css("border-color","#26A69A");
            }
            if(email=='') {
                $('#email').css("border-color","red");
            } else {
                $('#email').css("border-color","#26A69A");
            }
            if(name != '' && address != '' && email != '' && phone != '' && message != '' ) {
                $.ajax({
                    url: "mail.php",
                    type: "post",
                    data: {name: name, email: email, address: address, phone: phone, message: message},
                    success: function (data) {
                        $('#contact-thankyou').html("Thank you.  We've received your Query.").fadeOut(5000);
                        $('#contact-form').trigger("reset");
                    }

                });
            }else{
                $('#contact-error').html("All fields are required").fadeOut(5000);
            }


        });
    });
</script>

      
<?php include('includes/footer.php');?>
