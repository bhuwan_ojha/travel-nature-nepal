<div class="row grey lighten-5 no-margin" style="padding: 30px 0;margin-bottom: -20px;">
  <div class="container">
    <div class="menu-box">
      <div class="row no-margin">
        <div class="col l12 s12">
          <h5 class="section-heading grey-text text-darken-3">Our recomendation</h5>
        </div>
        <div class="col l12 s12">
          <div id="owl-demo-2" class="owl-carousel owl-theme owl-demo">
              <?php
              require_once "Class/Connection.php";
              require_once "Class/Places.php";

              $objPlaces = new Places();
              $data =$objPlaces->viewRecommendedPlacesIndex();

              if($data!=0){
              foreach($data as $value){
              ?>
            <!--item 1-->
            <div class="item col l12 s12">
              <div class="card z-depth-0">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="<?php echo BASE_URL; ?>uploads/Places/<?php echo $value->image;?>" height="150px">
                </div>
                <div class="card-content">
                  <ul class="menu-card-inside">
                    <li class="travel-name"><a href="#" class="blue-text text-darken-4"> <?php echo $value->destination_name;?></a></li>
                    <li>
                      <ul class="inline-ul">
                        <!--<li class="travel-price grey-text"><i class="fa fa-money"></i> 3500</li> |
                        <li class="travel-price grey-text"><i class="fa fa-heart-o"></i> 350</li> |  
                        <li class="travel-price grey-text"><i class="fa fa-group"></i> 10</li>   -->
                      </ul>
                    </li>
                    <li class="travel-details grey-text text-darken-2"><?php echo substr($value->description, 0, 87); ?>...</li>
                  </ul>
                    <a href="<?php echo BASE_URL;?>place-detail/<?php echo base64_encode($value->slug);?>" class="btn-sm-out-line white-text">Vew Details</a>
                </div>
                <div class="card-reveal">
                  <span class="card-title blue-text text-darken-4"><?php echo $value->destination_name;?><i class="fa fa-times right"></i></span>
                  <p><?php echo ($value->description); ?></p>
                </div>
              </div>
            </div>
              <?php }}else{?>
                  <div class="item col l12 s12">
              <div class="card z-depth-1">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="<?php echo BASE_URL; ?>assets/img/nepal_kathmandu.jpg">
                </div>
                <div class="card-content">
                  <ul class="menu-card-inside">
                    <li class="travel-name"><a href="place-details.php" class="blue-text text-darken-4"> Lumbini</a></li>
                    <li>
                      <ul class="inline-ul">
                        <li class="travel-price grey-text"><i class="fa fa-money"></i> 3500</li> |
                        <li class="travel-price grey-text"><i class="fa fa-heart-o"></i> 350</li> |
                        <li class="travel-price grey-text"><i class="fa fa-group"></i> 10</li>
                      </ul>
                    </li>
                    <li class="travel-details grey-text text-darken-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                  </ul>
                  <a href="#" class="btn-sm-out-line white-text">Vew Details</a>
                </div>
                <div class="card-reveal">
                  <span class="card-title blue-text text-darken-4">Card Title<i class="fa fa-times right"></i></span>
                  <p>Here is some more information about this product that is only revealed once clicked on.</p>
                </div>
              </div>
            </div>
              <?php } ?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>