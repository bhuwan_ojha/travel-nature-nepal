	<div class="row no-margin">
		<div class="container blue-text text-darken-4">
			<div class="col l4 m4 s12">
				<div class="row main-info">
					<div class="col s3">
						<i class="fa fa-heart-o"></i>
					</div>
					<div class="col s9">
						<h4 class="no-margin">QUALITY TRAVEL</h4>
						<p class="no-margin"></p>
					</div>
				</div>
			</div>
			<div class="col l4 m4 s12">
				<div class="row main-info center-border ">
					<div class="col s3">
						<i class="fa fa-retweet"></i>
					</div>
					<div class="col s9">
						<h4 class="no-margin">PROFESSIONAL GUIDE</h4>
						<p class="no-margin"></p>
					</div>
				</div>
			</div>
			<div class="col l4 m4 s12">
				<div class="row main-info ">
					<div class="col s3">
						<i class="fa fa-coffee"></i>
					</div>
					<div class="col s9">
						<h4 class="no-margin">FOOD & HOTELS</h4>
						<p class="no-margin"></p>
					</div>
				</div>
			</div>
		</div>
	</div>