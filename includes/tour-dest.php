<style type="text/css">
h5 {
  position: relative;
}


h5.one:before {
  content: "";
  display: block;
  border-top: solid 1px #BDBDBD;
  width: 100%;
  height: 1px;
  position: absolute;
  top: 50%;
  z-index: 1;
}

h5.one span {
  background: #EDEDED;
  padding-right: 15px;
  position: relative;
  z-index: 5;
}

</style>
<div class="row no-margin">
  <div class="container">
    <div class="menu-box">
      <div class="row">
        <div class="col l12 s12">
          <h5 class="section-heading one red-text text-darken-4">
          <span>Top destination</span> </h5>
        </div>
        <div class="col l12 s12">
          <div id="owl-demo-1" class="owl-carousel owl-theme">
              <?php
              require_once "Class/Connection.php";
              require_once "Class/Places.php";

              $objPlaces = new Places();
              $data =$objPlaces->viewTopPlacesIndex();
              if($data!=0){
              foreach($data as $value){
              ?>
            <div class="item col l12 s12">
              <div class="card z-depth-0 z-depth-0">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="<?php echo BASE_URL; ?>uploads/Places/<?php echo $value->image;?>" height="200px">
                </div>
                <div class="card-content">
                  <ul class="menu-card-inside">
                    <li class="travel-name"><a href="#" class="blue-text text-darken-4"> <?php echo $value->destination_name; ?><span class="right"><small class="grey-text"></small></span></a></li>
                    <li>

                      <!--<ul class="inline-ul">
                        <li class="travel-price grey-text"><i class="fa fa-star"></i></li> 
                        <li class="travel-price grey-text"><i class="fa fa-star"></i></li>  
                        <li class="travel-price grey-text"><i class="fa fa-star"></i></li>                         
                        <li class="travel-price grey-text"><i class="fa fa-star"></i></li>  
                        <li class="travel-price grey-text"><i class="fa fa-star"></i></li>                     
                      </ul>-->
                    </li>
                    <li class="travel-details grey-text text-darken-2"></li>
                  </ul>
                    <a href="<?php echo BASE_URL;?>place-detail/<?php echo base64_encode($value->slug);?>" class="btn-sm-out-line white-text">Vew Details</a>
                </div>
              </div>
            </div>
              <?php }} ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>