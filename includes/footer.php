<footer id="contact" class="page-footer grey lighten-5">
    <div class="overlay-ablack" style="padding: 30px 0 0px;">
        <div class="container">
            <div class="row no-margin" style="padding-bottom: 30px;background-image: url('<?php echo BASE_URL; ?>assets/img/about-side.png');background-size:contain;background-position:bottom;background-repeat: no-repeat;">
                <div class="col l4 m10 s12">
                    <div class="sec-heading">
                        <h5 class="grey-text text-darken-4">About Us</h5>
                    </div>
                    <ul>
                        <?php require_once "Class/Connection.php";
                        require_once "Class/Page.php";

                        $objPage = new Page();
                        $data = $objPage->ViewAboutContents();
                        if($data!=0){
                            foreach ($data as $value){
                                echo substr($value->page_desc,0,400);
                            }}
                            ?>...
                    </ul>
                </div>
                <div class="col l3 m4 s12">
                    <div class="sec-heading">
                        <h5 class="grey-text text-darken-4">Get In Touch</h5>
                    </div>
                    <ul>
                        <li class="grey-text text-darken-3">Travel Nature Nepal Travel & Tours</li>
                        <li class="grey-text text-darken-3">Kathmandu,Nepal</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-phone"></i> (123)456789</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-inbox"></i> info@travelnaturenepal.com</li>
                    </ul>
                </div>
                <div class="col l2 m4 s12">
                    <div class="sec-heading">
                        <h5 class="grey-text text-darken-4">Links</h5>
                    </div>
                    <ul>
                        <a href="<?php echo BASE_URL; ?>home" class="blue-text text-darken-4"><li> Home</li></a>
                        <a href="<?php echo BASE_URL; ?>destinations" class="blue-text text-darken-4"><li> Destinations</li></a>
                        <a href="<?php echo BASE_URL; ?>gallery" class="blue-text text-darken-4"><li> Gallery</li></a>
                        <a href="<?php echo BASE_URL; ?>about" class="blue-text text-darken-4"><li> About</li></a>
                        <a href="<?php echo BASE_URL; ?>contact" class="blue-text text-darken-4"><li> Contact</li></a>
                    </ul>

                </div>
                <div class="col l3 m4 s12">
                    <div class="sec-heading">
                        <h5 class="grey-text text-darken-4">Social</h5>
                    </div>
                    <div class="col s12">
                        <div class="row">
                            <div class="top-stories-box">
                                <ul class="social-links-down">
                                    <li><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook blue-text text-darken-4"></i></a></li>
                                    <li><a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter blue-text text-darken-4"></i></a></li>
                                    <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus blue-text text-darken-4"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright z-depth-2" style="background-color:#7db338">
            <div class="container black-text" >
                Travel Nature Nepal Pvt Ltd &copy; <?php echo date('Y');?> <span class="blue-text text-darken-4"></span>
                <a class="right" style="color: #f5f5f5" href="#top" >Top <i class="fa fa-arrow-up"></i></a>
            </div>
        </div>
    </div>
</footer>

<!--Import jQuery before materialize.js-->

<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jQuery.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jPages.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jPages.js"></script>

<script src="assets/js/mrova-feedback-form.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/materialize.js"></script>
<script type='text/javascript' src='<?php echo BASE_URL; ?>assets/js/jquery.validationEngine.js'></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.validationEngine-en.js"></script>
<!--OWL CAROUSEL-->
<script src="<?php echo BASE_URL; ?>assets/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo-1").owlCarousel({
            itemsCustom : [
                [0, 1.3],
                [450, 2],
                [600, 3.2],
                [700, 3.2],
                [1000, 4],
                [1200, 4],
                [1400, 4],
                [1600, 4]
            ],
            autoPlay : false,
            stopOnHover : true,
            navigation:false,
            pagination:false,
            paginationSpeed : 1000,
            goToFirstSpeed : 2000,
            autoHeight : true,
            transitionStyle:"fade",
        });
    });
    $(document).ready(function() {
        var owl = $("#owl-demo-1");
        owl.owlCarousel();
        // Custom Navigation Events
        $(".next-1").click(function(){
            owl.trigger('owl.next');
        })
        $(".prev-1").click(function(){
            owl.trigger('owl.prev');
        })
    });
</script>
<script>
    $(document).ready(function() {
        $("#owl-demo-2").owlCarousel({
            itemsCustom : [
                [0, 1.5],
                [450, 2],
                [600, 3],
                [700, 3],
                [1000, 4],
                [1200, 4],
                [1400, 4],
                [1600, 4]
            ],
            autoPlay : false,
            stopOnHover : true,
            navigation:false,
            pagination:false,
            paginationSpeed : 1000,
            goToFirstSpeed : 2000,
            autoHeight : true,
            transitionStyle:"fade"
        });
    });
    $(document).ready(function() {
        var owl = $("#owl-demo-2");
        owl.owlCarousel();
        // Custom Navigation Events
        $(".next-1").click(function(){
            owl.trigger('owl.next');
        })
        $(".prev-1").click(function(){
            owl.trigger('owl.prev');
        })
    });
</script>
<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initMap() {
        var origin_place_id = null;
        var destination_place_id = null;
        var travel_mode = google.maps.TravelMode.DRIVING;
        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        });
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);

        var origin_input = document.getElementById('origin-input');
        var destination_input = document.getElementById('destination-input');


        var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
        origin_autocomplete.bindTo('bounds', map);
        var destination_autocomplete =
            new google.maps.places.Autocomplete(destination_input);
        destination_autocomplete.bindTo('bounds', map);

        function expandViewportToFitPlace(map, place) {
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        }

        origin_autocomplete.addListener('place_changed', function() {
            var place = origin_autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            expandViewportToFitPlace(map, place);

            // If the place has a geometry, store its place ID and route if we have
            // the other place ID
            origin_place_id = place.place_id;
            route(origin_place_id, destination_place_id, travel_mode,
                directionsService, directionsDisplay);
        });

        destination_autocomplete.addListener('place_changed', function() {
            var place = destination_autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            expandViewportToFitPlace(map, place);

            // If the place has a geometry, store its place ID and route if we have
            // the other place ID
            destination_place_id = place.place_id;
            route(origin_place_id, destination_place_id, travel_mode,
                directionsService, directionsDisplay);
        });

        function route(origin_place_id, destination_place_id, travel_mode,
                       directionsService, directionsDisplay) {
            if (!origin_place_id || !destination_place_id) {
                return;
            }
            directionsService.route({
                origin: {'placeId': origin_place_id},
                destination: {'placeId': destination_place_id},
                travelMode: travel_mode
            }, function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }
    }
</script>
<script type="text/javascript">
    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false,
            hover: true,
            gutter: 0,
            belowOrigin: true,
            alignment: 'left'
        }
    );
</script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/app.js"></script>