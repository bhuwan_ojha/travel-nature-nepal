<?php
	require_once 'functions.php';
	
	define("BASE_URL", base_url());
?>
<!DOCTYPE html>
  <html>
    <head>    
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">      
      <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/materialize.css"  media="screen,projection"/>
      <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/fonts/font-awesome-4.6.3/css/font-awesome.min.css">     
      <link href="<?php echo BASE_URL; ?>assets/css/owl.carousel.css" rel="stylesheet">
      <link href="<?php echo BASE_URL; ?>assets/css/owl.theme.css" rel="stylesheet">     
      <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/mrova-feedback-form.css" type="text/css"/>      
      <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/validationEngine.jquery.css" type="text/css"/>     
      <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/custom.css">



        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jQuery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jPages.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jPages.js"></script>
        <script type='text/javascript' src='<?php echo BASE_URL; ?>assets/js/jquery.validationEngine.js'></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jQuery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.validationEngine-en.js"></script>

    
        <?php

        require_once"Class/Connection.php";

        require_once"Class/Seo.php";

        $title = $_SERVER['REQUEST_URI'];

        $title = basename($title);

        $objSeo = new Seo();

        $data = $objSeo->ViewSeoIndex($title);

        if($data !=0){

            foreach($data as $seo){

                ?>

                <title>Travel Nature Nepal - <?php echo $seo->name;?></title>

                <meta name="description" content="<?php echo $seo->description;?>">

                <meta name="keywords" content="<?php echo $seo->keywords;?>">

            <?php }}?>
    </head>

    <body>

  <div id="mrova-feedback" style="height: 450px;"  class="z-depth-5 blue lighten-5">
    <div id="mrova-contact-thankyou" style="color: green;margin-left: 9px;"></div>
    <div id="mrova-contact-error" style="color: red;margin-left: 9px;"></div>
    <div id="mrova-form">
      <form class="col s12" id="mrova-contactform" action="#" method="post">
      <div class="col s12 section-heading" style="padding:5px 9px;">
        <h5 class="grey-text text-darken-4" style="font-size: 1.2rem;line-height: 1rem;">contact form</h5>
      </div>
      <div class="row no-margin">
        <div class="col s12">
          <label for="mrova-name">Your Name*</label> 
          <input type="text" name="mrova-name" placeholder="Your Name" class="required form-style-1 " id="mrova-name" value="">

        </div>
        <div class="col s12">
          <label for="mrova-email">Email*</label> 
          <input type="text" name="mrova-email" class="required form-style-1 " placeholder="Your Email" id="mrova-email" value="">

        </div>
        <div class="col s12">
          <label for="mrova-message">Message*</label>
            <textarea class="required materialize-textarea form-style-1 " id="mrova-message" name="mrova-message" placeholder="Message"></textarea>

        </div>
        <div class="col s12">
          <button type="submit"  id="submit"  class="btn-flat red white-text">SEND</button>
        </div>
        </div>
      </form>
    </div>

  <div class="fixed-action-btn">
    <a class="btn-floating btn-large green accent-4" id="mrova-img-control">
      <i class="large material-icons">mode_edit</i>
    </a>
    </div>
  </div>
<?php
$slug1 = basename($_SERVER['REQUEST_URI']);
$slug =  strtok($slug1, '?');
?>
<script>
    $(function(){
        $("#mrova-contactform").validationEngine({
            promptPosition:"topLeft"
        });

        $('#mrova-contactform').on("submit", function(e){
            e.preventDefault();

            var name = $('#mrova-name').val();
            var email = $('#mrova-email').val();
            var message = $('#mrova-message').val();

            if (name == '') {
                $('#mrova-name').css("border-color","red");
            } else {
                $('#mrova-name').css("border-color","#26A69A");
                $('name-error').show();
            }
            if(message=='') {
                $('#mrova-message').css("border-color","red");
            } else {
                $('#mrova-message').css("border-color","#26A69A");
            }
            if(email=='') {
                $('#mrova-email').css("border-color","red");
            } else {
                $('#mrova-email').css("border-color","#26A69A");
            }
            if(name != '' && email != '' && message != '' ) {

                $.ajax({
                    url: "ajax/contact-submit.php",
                    type: "post",
                    data: {name: name, email: email, message: message},
                    success: function (data) {
                        $('#mrova-contact-thankyou').html("Thank you.  We've received your feedback.").fadeOut(5000);
                        setTimeout(function () {
                            $('#mrova-img-control').trigger('click');
                        }, 2000);
                        $('#mrova-name').val('');
                        $('#mrova-email').val('');
                        $('#mrova-message').val('');
                    }

                });
            }else{
                $('#mrova-contact-error').html("All fields are required").fadeOut(5000);
            }

        });
    });

   var slug ="<?php echo $slug;?>"

    $(function () {

        $('[data-toggle="tooltip"]').tooltip()

        var menuObj = $("a[href="+slug+"]");

        if(menuObj.length) {
            menuObj.closest("li").addClass("active");
            if (menuObj.closest("li").parents("li").length){
                menuObj.closest("li").parents("li").find("ul").show();
                menuObj.closest("li").parents("li").addClass("active");
            }
        }
    });

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=134259316916399";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
