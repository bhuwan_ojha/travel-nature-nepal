<div class="slider" style="margin-top: -5px;">
    <ul class="slides">
        <?php
        require_once "Class/Connection.php";
        require_once "Class/Image.php";

        $objImage = new Image();
        $data = $objImage->viewBannerInIndex();

        if($data!=0){

        foreach($data as $value){

        ?>
        <li>
            <img src="<?php echo BASE_URL;?>uploads/Banner/<?php echo $value->image;?>"> <!-- random image -->
            <div class="caption right-align z-depth-1">
                <?php if($value->title !=''){ ?>
                <div class="front-banner-heading">
                    <p class="no-margin red-text"></p>
                    <h2 class="grey-text text-lighten-3 no-margin"><?php echo $value->title;?></h2>
                </div>
                <?php } ?>
            </div>
            <?php }} ?>
        </li>
    </ul>
</div>

<style type="text/css">
    .slides li .caption{
        padding:0 15px;
        background-color: rgba(0,0,0,0.8);
    }
    .caption p{
        margin-top: 10px;
        margin-bottom: 0;
        text-transform: uppercase;
    }
</style>