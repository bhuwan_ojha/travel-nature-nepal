<style type="text/css">
  nav .dropdown-content li a{
    font-size: 14px;
    color: #212121;
  }
  @media screen and (min-width: 992px) {
    .navbar-fixeda{
      margin-bottom: 55px;
    }
  }
</style>
<div class="navbar-fixeda ">
<nav class="navbar z-depth-0">
    <div class="nav-wrapper">
      <ul class="tabs tabs-transparent white " style="border-bottom:1px solid rgba(0,0,0,0.2);line-height: 80px;height: 100px;">
        <div class="container">
          <a href="#" class="brand-logo"><img src="<?php echo BASE_URL;?>assets/img/logo.png" style="height: 111px;width: 172px;margin-top: -3px;" onclick="location.href='<?php echo BASE_URL;?>home'"></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse" style="color:green;margin-top: 15px;"><i class="material-icons">menu</i></a>
        </div>
      </ul>
      <div class="row "  style="background-color: #7db338;"><div class="container">
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li class="home"><a href="<?php echo BASE_URL;?>home">Home</a></li>
        <li class="destination"><a class='dropdown-button' data-activates='dropdownDest' href='#'>Destinations</a></li>
        <li class="gallery"><a href="<?php echo BASE_URL;?>gallery">Gallery</a></li>
        <li class="about"><a href="<?php echo BASE_URL;?>about">About Us</a></li>
        <li class="contact"><a href="<?php echo BASE_URL;?>contact">Contact Us</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo" >
          <li class="active"><a href="<?php echo BASE_URL;?>home">Home</a></li>
          <li><a href="<?php echo BASE_URL;?>destinations">Destinations</a></li>
          <li><a href="<?php echo BASE_URL;?>gallery">Gallery</a></li>
          <li><a href="<?php echo BASE_URL;?>about">About Us</a></li>
          <li><a href="<?php echo BASE_URL;?>contact">Contact Us</a></li>
      </ul>
      </div></div>
    </div>
  </nav>
</div>
<!-- Dropdown Structure -->
<ul id='dropdownDest' class='dropdown-content'>
    <?php
    require_once "Class/Connection.php";
    require_once "Class/Places.php";
    $objPlace = new Places();
    $data = $objPlace->getCategory();
    if($data !=0){
    foreach ($data as $value){
    ?>
    <li><a href="<?php echo BASE_URL;?>destination/<?php echo $value->category;?>"><?php echo $value->category;?></a></li>
   <?php }} ?>
</ul>
