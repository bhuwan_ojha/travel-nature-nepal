<!--Booking quote form-->
<div class="row no-margin" style="padding: 30px 0 20px;">
	<div class="container">
		<div class="row">
			<div class="col l8 m8 s12">
			          <h5 class="section-heading one red-text text-darken-4">
			          <span>Trip planner</span> </h5>
				<form action="#" method="POST">
					<div class="row">
						<div class="form-style-1 col s6">
							<label class="grey-text" for="pickup_location">Pick-up Location</label>
							<input id="origin-input" class="controls" type="text" placeholder="Enter an origin location">
						</div>									
						<div class="form-style-1 col s6">
							<label class="grey-text" for="pickup_date">Drop-up Location</label>
							<input id="destination-input" class="controls" type="text" placeholder="Enter a destination location">
						</div>
						<div class="col s3">
							<label class="grey-text">Date</label>
							<select class="browser-default">
							    <option value="1">Today</option>
							    <option value="2">Option 2</option>
							    <option value="3">Option 3</option>
							</select>
						</div>
						<div class="col s3">
							<label class="grey-text">Departure</label>
							<select class="browser-default">
							    <option value="1">Departed at</option>
							    <option value="2">Arrived at</option>
							</select>
						</div>
						<div class="col s2">
							<label class="grey-text">Hour</label>
							<select class="browser-default">
							    <option value="1">11</option>
							    <option value="2">12</option>
							</select>
						</div>
						<div class="col s2">
							<label class="grey-text">Minute</label>
							<select class="browser-default">
							    <option value="1">59</option>
							    <option value="2">60</option>
							</select>
						</div>
						<div class="col s2">
							<label class="grey-text">AM/PM</label>
							<select class="browser-default">
							    <option value="1">AM</option>
							    <option value="2">PM</option>
							</select>
						</div>
						<div class="form-style-1 col s12">
				          <label for="textarea1" class="grey-text">Custom message</label>
				          <textarea id="textarea1" style="height: 85px;"></textarea>
				        </div>
					</div>
					<button class="btn waves-effect waves-light red darken-4 grey-text text-lighten-4 z-depth-0" type="submit" name="action">Plan trip
					    <i class="material-icons right">send</i>
					</button>
				</form>
			</div>

			<div class="col l4 m4 s12">
		        <h5 class="section-heading one red-text text-darken-4">
          		<span>Trending</span> </h5>
				<div class="collection z-depth-0">
                    <?php
                    require_once "Class/Connection.php";
                    require_once "Class/Places.php";

                    $objPlaces = new Places();
                    $data =$objPlaces->viewTrendingPlaces();
                    if($data!=0){
                    foreach($data as $value){
                    ?>
			        <a href="#!" class="collection-item blue-text text-darken-4"> <?php echo $value->destination_name; ?></a>
		<?php }} ?>
			    </div>
			</div>
		</div>
	</div>
</div>
	<div id="map" style="visibility: hidden;"></div>

