<?php include('includes/header.php');?>

    <?php include('includes/nav.php');?>

    <div class="bread-bg-img">
      <div class="overlay-white-right">
        <div class="container">
          <div class="bread-bg-details">
            <h3 class="no-margin left-align grey-text text-darken-3">About Us</h3>

          </div>
        </div>
      </div>
    </div>

    <section class="news-detail-sec white" style="padding: 30px 0;">
        <div class="container">
          <div class="row">
            <div class="col m8">
              <div class="col m11">
                <div class="row">
                <h5 class="section-heading grey-text text-darken-2">We are Travel Nature Nepal</h5><br>
                   <div class="row">
                     <div class="col s12">
                         <?php
                         require_once "Class/Connection.php";
                         require_once "Class/Page.php";

                         $objPage = new Page();
                         $data = $objPage->ViewAboutContents();
                         if($data!=0){
                             foreach($data as $value){

                         ?>
                         <?php if($value->image !=''){ ?>
                      <img class="materialboxed responsive-img" src="uploads/PageImage/<?php echo $value->image;?>">
                         <?php }else{ ?>
                             <img class="materialboxed responsive-img" src="assets/img/ab-bg.jpg">
                         <?php } ?>
                    </div>
                    <div class="col s12" style="margin-bottom:30px;">
                      <p><?php echo $value->page_desc;?></p> </div>

                       <?php }} ?>

                    <div class="col s12 donation-box">
                      <div class="row">
                        <div class="col m4">
                          <img class="responsive-img" src="assets/img/med-img-1.jpg">
                          <div class="news-details"><p class="no-margin donor-name">Luxurious Hotels</p>
                            <div class="news-line"></div>
                          </div>
                        </div>
                        <div class="col m4">
                          <img class="responsive-img" src="assets/img/med-img-2.jpg">
                          <div class="news-details"><p class="no-margin donor-name">Real Tour</p>
                            <div class="news-line"></div>
                          </div>
                        </div>
                        <div class="col m4">
                          <img class="responsive-img" src="assets/img/med-img-3.jpg">
                          <div class="news-details"><p class="no-margin donor-name">Unique Experience</p>
                            <div class="news-line"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                   </div>
                </div>
              </div>
            </div>


            <div class="col m4 grey lighten-4" style="border:1px solid #F5F5F5;margin-bottom: 15px;">
              <div class="col s12">
                <div class="left-align">
                  <h5 class="section-heading grey-text text-darken-2">Our Facebook Page</h5>
                </div>
              </div>
                <div class="fb-post" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/" data-width="433" data-show-text="true"><blockquote cite="https://www.facebook.com/20531316728/posts/10154009990506729/" class="fb-xfbml-parse-ignore">Posted by <a href="https://www.facebook.com/facebook/">Facebook</a> on&nbsp;<a href="https://www.facebook.com/20531316728/posts/10154009990506729/">Thursday, August 27, 2015</a></blockquote></div>
              </div>
             <!-- <div class="col m4" style="border:1px solid #F5F5F5;background-color:#F9F9F9;padding: 9px 0 15px;margin-bottom: 15px;">
                <div class="col s12">
                  <div class="left-align">
                    <h5 class="section-heading grey-text text-darken-2">Our location</h5>
                  </div>
                </div>
                <div class="col s12">
                  <div class="news">
                    <div class="top-stories-box">
                      <ul>
                        <li class="grey-text text-darken-3">Demo Travel Agency, Nepal</li>
                        <li class="grey-text text-darken-3">Your Address, Kathmandu</li>
                        <li class="grey-text text-darken-3">Around Somewhere, 7</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-phone"></i> (123)456789</li>
                        <li class="grey-text text-darken-3"><i class="fa fa-inbox"></i> info@yourdomain.com</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>  -->
          </div>
        </div>
      </section>
	  <?php include('includes/main-information.php');?>


      
<?php include('includes/footer.php');?>
