<?php include('includes/header.php');?>

    <?php include('includes/nav.php');?>

    <div class="bread-bg-img">
      <div class="overlay-white-right">
        <div class="container">
          <div class="bread-bg-details">
            <h3 class="no-margin left-align grey-text text-darken-3">Our Gallery</h3>

          </div>
        </div>
      </div>
    </div>


    <section class="gallery-page-sec" style="padding: 30px 0;">
      <div class="container">
        <div class="row">
          <div class="col s12">
              <h5 class="section-heading grey-text text-darken-2">Our latest gallery</h5>
          </div>
          <div class="col l12 s12">
            <div class="row">
                <ul id="itemContainer">
                <?php
                require_once"Class/Connection.php";
                require_once"Class/Image.php";

                $objGallery = new Image();
                $data =$objGallery->viewGallery();
                if($data!=0){
                    foreach($data as $value){

                    ?>
              <div class="col m4 s6">
                <div class="card">
                  <div class="card-image">
                    <div  class="responsive-img" style="background-image: url('uploads/Banner/<?php echo $value->image;?>');height: 200px;background-position: center;background-size: cover;">
                      <div class="overlay-black">
                        <!--<a href="gallery-detail.php" class="card-title card-title-fapla-gallery">New gallery</a href="#!">-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                    <?php }} ?>
                    </ul>









            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="container">
      <div class="row">
        <ul class="pagination center-align">
            <div class="holder"></div>
        </ul>
      </div>
    </div>

    <script>
        /* when document is ready */
        $(function(){

            /* initiate the plugin */
            $("div.holder").jPages({
                containerID  : "itemContainer",
                perPage      : 6,
                startPage    : 1,
                startRange   : 1,
                midRange     : 5,
                endRange     : 1
            });

        });
    </script>

<?php include('includes/footer.php');?>