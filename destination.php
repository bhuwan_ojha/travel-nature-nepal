<?php include('includes/header.php');?>

    <?php include('includes/nav.php');?>

    <div class="bread-bg-img">
      <div class="overlay-white-right">
        <div class="container">
          <div class="bread-bg-details">
            <h3 class="no-margin left-align grey-text text-darken-3">Destination</h3>
            <p class="grey-text text-darken-2"></p>
          </div>
        </div>
      </div>
    </div>

    <section class="news-detail-sec white" style="padding: 30px 0;">
        <div class="container">
          <div class="row">
              <div class="col m3 s12 grey lighten-4" style="border:1px solid #F5F5F5;padding: 9px 0 15px;margin-bottom: 5px;">
                <div class="col s12">
                  <div class="left-align">
                    <h5 class="section-heading grey-text text-darken-2"><small>Destinations</small></h5>
                  </div>

                  <div class="row no-margin">
                    <div class="collection z-depth-0">
                        <?php
                        require_once "Class/Connection.php";
                        require_once "Class/Places.php";

                        $objPlaces = new Places();
                        $data =$objPlaces->viewAllPlacesSideList();
                        if($data!=0){
                        foreach($data as $value){
                        ?>
                      <a href="#!" class="collection-item blue-text text-darken-4"><?php echo $value->destination_name;?></a>

                        <?php }} ?>
                    </div>
                  </div>

                </div>

              </div>


            <div class="col m8 offset-m1 s12">
              <div class="row">
              <div class="col s12" style="margin-top: 0px;margin-bottom: 15px;">
                  <h4 class="section-heading grey-text text-darken-2">Top <?php echo urldecode (basename($_SERVER['REQUEST_URI']));?> destination </h4>
              </div>
                  <ul id="itemContainer">
                  <?php
                  require_once "Class/Connection.php";
                  require_once "Class/Places.php";

                  $objPlaces = new Places();
                  $data =$objPlaces->viewAllPlacesbyCategory();
                  if($data!=0){
                  foreach($data as $value){
                  ?>
              <div class="col s12 news-box">
                <div class="row">
                  <div class="col m5">
                    <img src="<?php echo BASE_URL; ?>uploads/Places/<?php echo $value['image'];?>" class="responsive-img">
                  </div>
                  <div class="col m7">
                    <div class="news-details">
                      <a href="place-details.php"><h5 class="grey-text text-darken-3 uppercase"><?php echo $value['destination_name'];?></h5></a>
                     <!-- <span><i class="fa fa-calendar-o"></i> Date: 2016-JUL-8</span>-->
                      <div class="news-line"></div>
                      <p>
                          <?php echo substr($value['description'], 0,  248 ); ?>...
                      </p>
                      <a href="<?php echo BASE_URL;?>place-detail/<?php echo base64_encode($value['slug']);?>" class="btn-flat red darken-4 white-text">Read More</a>
                    </div>
                  </div>
                </div>
              </div>
                  <?php }} ?>
                  </ul>

            <div class="holder"></div>

              <!--<div class="holder col s12">
                <div class="row">
                  <ul class="pagination left-align">
                      <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                      <li class="active"><a href="#!">1</a></li>
                      <li class="waves-effect"><a href="#!">2</a></li>
                      <li class="waves-effect"><a href="#!">3</a></li>
                      <li class="waves-effect"><a href="#!">4</a></li>
                      <li class="waves-effect"><a href="#!">5</a></li>
                      <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                  </ul>
                </div>
              </div>-->
            </div>
            </div>
          </div>
        </div>
      </section>
	  <?php include('includes/main-information.php');?>
      <?php include('includes/tour-dest.php');?>


      
<?php include('includes/footer.php');?>
<script>
    /* when document is ready */
    $(function(){

        /* initiate the plugin */
        $("div.holder").jPages({
            containerID  : "itemContainer",
            perPage      : 3,
            startPage    : 1,
            startRange   : 1,
            midRange     : 5,
            endRange     : 1
        });

    });
</script>